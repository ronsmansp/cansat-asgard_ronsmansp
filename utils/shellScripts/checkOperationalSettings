#!/bin/bash
# Check the various conditional compilation symbols are ok for operational
# on-board software. 

##### Complete those constants to tune the checks ###########

LIB_LIST="cansatAsgardCSPU DebugCSPU EEPROM_CSPU IsatisCSPU TimerCSPU"

SYMBOLS_TO_DEFINE_IN_LIBS=" \
	INIT_SERIAL PRINT_DIAGNOSTIC_AT_INIT \
	DBG_CHECK_SPI_DEVICES PRINT_ACQUIRED_DATA_CSV DEBUG "

SYMBOLS_TO_DEFINE_IN_CONFIG=" DEBUG "

SYMBOLS_TO_UNDEFINE_IN_LIBS="USE_TIMER USE_ASSERTIONS"

SYMBOLS_TO_UNDEFINE_IN_CONFIG="   IGNORE_EEPROMS USE_ASSERTIONS \
	USE_TIMER USE_MINIMUM_DATARECORD \
	PRINT_ACQUIRED_DATA "

DBG_SYMBOLS_ALLOWED_TO_1="DBG_DIAGNOSTIC"

CONST_TO_CHECK="	serialBaudRate=19200 \
	serial1BaudRate=19200 \
	GPY_DefaultVocValue=0.43 \
	GPY_SensitivityInMicroGramPerV=200 \
	GPY_MinSignificantDeltaV=0.15 \
	GPY_MaxDeltaInAverageSet=0.4 \
	GPY_NbrOfReadingsToAverage=8 \
	GPY_ForceLowDensityToZero=false \
	IsatisAcquisitionPeriod=400 \
	IsatisSensorsWarmupDelay=1000 \
	MinSpeedToStartCompaign=1.2 \
	NumSamplesToStartCamapaign=(1000/IsatisAcquisitionPeriod) \
	SD_RequiredFreeMegs=100 \
	GPY_DelayBeforeSamplingInMicroSec=280 \
	GPY_DelayAfterSamplingInMicroSec=40 \
	GPY_DelayLED_OffInMicroSec=9680 \
	GPY_RepeatBadQualityReadings=false \
	I2C_lowestAddress=0x4b \
	I2C_highestAddress=0x7F \
	I2C_BMP_SensorAddress=0x77 \
	unusedAnalogInPinNumber=0 \
	GPY_dataOutPinNbr=A6 \
	GPY_LED_VccPinNbr=13 \
	Fan_DigitalPin=14 \
	RF_TransmissionDelayEveryTwoNumbers=50"

##### Do not change anything below this line, unless you know what you're doing

ME=`basename "$0"`

#perform check on constants values in IsatisConfig.h
function checkIsatisConfigConstants() {
  FILE=$LIB_DIR/IsatisCSPU/src/IsatisConfig.h
  if [ ! -f $FILE ] ;
  then
    echo "*** ERROR: cannot find file $FILE"
    return
  fi
  for constValue in $CONST_TO_CHECK ;
  do
     const=$(echo $constValue | cut -d= -f1)
     val=$(echo $constValue | cut -d= -f2)
     echo -n "  Checking $const... "
     check=`grep " *const.*$const *= *$val *;" $FILE`
     if [ -z "$check" ] ;
     then
       echo
       echo "*** Error: constant $const should be $val"
       grep " *const .*$const *=" $FILE | cut -c 1-75
     else
       echo OK.
     fi
  done
}

# perform checks on the DBG_xxxx defines in current dir.
function checkDbgDefines() {
   cmd="grep \" *# *define *DBG_.* *1\" *.cpp *.h 2>/dev/null"
  check=`eval $cmd`
  for symbol in $DBG_SYMBOLS_ALLOWED_TO_1 ;
  do
    check=`echo "$check" | grep -v $symbol`
  done
  if [ ! -z "$check" ];
  then
    echo "*** Error: symbol(s) should be defined to 0"
    echo "$check" | cut -c 1-75
  fi
}

# check general purpose defines in current dir
function checkGeneralCppDefines() {  
  for symbol in $SYMBOLS_TO_DEFINE_IN_LIBS ;
  do
    cmd="grep \" *// *# *define *$symbol\" *.cpp *.h 2>/dev/null"
    check=`eval $cmd`
    if [ ! -z "$check" ];
    then
      echo "*** Error: $symbol should be defined "
      echo "$check" | cut -c 1-75 
    else
      echo "  $symbol: OK"
    fi
#
#    cmd="grep \"define *$symbol\" *.cpp *.h 2>/dev/null"
#    check=`eval $cmd`
#    if [ ! -z "$check" ];
#    then
#      echo Check $symbol is defined wherever it should:
#      echo "$check"| cut -c 1-75 
#    fi
  done
  
  for symbol in $SYMBOLS_TO_UNDEFINE_IN_LIBS ;
  do
    cmd="grep \"^# *define *$symbol\" *cpp *h 2>/dev/null"
    check=`eval $cmd`
    if [ ! -z "$check" ];
    then
      echo "*** Error: $symbol should be undefined!"
      echo "$check" | cut -c 1-75 
    fi
  done
}

# perform checks on $THIS_LIBRARY directory
function checkLibrary() {
  pushd $THIS_LIB >/dev/null 2>&1
  checkGeneralCppDefines
  checkDbgDefines
  popd >/dev/null 2>&1
}

echo
echo "$ME: ISATIS configuration check utility"
echo

#### 1. Define location of sources root directory.
SRC_DIR=$HOME/Documents/Arduino/cansat-asgard
if [ ! -d $SRC_DIR ]; then 
  SRC_DIR=$HOME/Documents/cansat-asgard
fi
if [ ! -d $SRC_DIR ]; then 
  echo -n Identifying source directory... 
  SRC_DIR=`find $HOME -type d -name "cansat-asgard" -print`
  echo OK
fi
echo Checking configuration in dir $SRC_DIR...

LIB_DIR=$SRC_DIR/libraries
if [ ! -d $LIB_DIR ]; then
  echo "*** Error: no $DIR_LIB found. Aborted"
  exit -1
fi 

#### 2. Explore all relevant librairies.
for lib in $LIB_LIST;
do
  THIS_LIB=$LIB_DIR/$lib/src
  echo
  echo Checking library $lib...
  if [ ! -d $THIS_LIB ] ;
  then
    echo
    echo "  *** Cannot find dir $THIS_LIB. Library skipped."
    continue
  fi
  checkLibrary
done
echo
echo Checking IsatisConfig.h constants...
checkIsatisConfigConstants

echo
echo ---- $ME: End of job.
