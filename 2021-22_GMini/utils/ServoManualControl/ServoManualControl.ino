#include "CSPU_Test.h"
#include "DebugCSPU.h"
#include "ServoLatch.h"

ServoLatch myServo;
int neutralPosition = 83;
int amplitude = 30;

void setup() {
  DINIT(115200);
  myServo.begin(9);
  myServo.configure(neutralPosition, amplitude);
  Serial << "Manual latch control program"<< ENDL;
  Serial << "----------------------------"<< ENDL;
  Serial << "Write C to increase the neutral position by one degree."<< ENDL;
  Serial << "Write c to decrease the neutral position by one degree."<< ENDL;
  Serial << "Write A to incraese the amplitude by one degree."<< ENDL;
  Serial << "Write a to decrease the amplitude by one degree."<< ENDL;
  Serial << "Write u to call the unlock function."<< ENDL;
  Serial << "Write l to call the lock function." << ENDL;
  Serial << "Write n to call the neutral function."<< ENDL;
}

void loop() {
  char c = CSPU_Test::keyPressed() ;
  switch (c) {
    case 'C':
      neutralPosition++;
      Serial << "neutral position now at " << neutralPosition << " degrees. " << ENDL;
      myServo.configure(neutralPosition, amplitude);
      myServo.neutral();
      break;
    case 'c':
      neutralPosition--;
      Serial << "neutral position now at " << neutralPosition << " degrees. " << ENDL;
      myServo.configure(neutralPosition, amplitude);
      myServo.neutral();
      break;
    case 'A':
      amplitude++;
      Serial << "amplitude now at " << amplitude << " degrees. " << ENDL;
      myServo.configure(neutralPosition, amplitude);
      myServo.lock();
      break;
    case 'a':
      amplitude--;
      Serial << "amplitude now at " << amplitude << " degrees. " << ENDL;
      myServo.configure(neutralPosition, amplitude);
      myServo.lock();
      break;
    case 'u':
      Serial.println("Unlock");
      myServo.unlock();
      break;
    case 'l':
      Serial.println("Lock");
      myServo.lock();
      break;
    case 'n':
      Serial.println("Neutral");
      myServo.neutral();
      break;
    default:
      ;// do nothing
  }

}
