/*
    Sketch to read whatever strings are received on the RX pin
    (Serial UART)  and store it on SD card.
*/

#include "HardwareScanner.h"
#include "IsatisConfig.h"
#include <SoftwareSerial.h>
#include "SD_Logger.h"

const byte RX_Pin = 9;
const byte TX_Pin = 3;
const byte CS_Pin = 10; // Chip select pin for SD Card.

SoftwareSerial gtSerial(RX_Pin, TX_Pin); // Arduino RX, Arduino TX
const int BufferSize = 100;

char buffer[BufferSize];
SD_Logger logger;
bool cardOK = false;

void setup() {
  Serial.begin(serialBaudRate); // The baud rate to communicate on USB port
  while (!Serial) ;
  pinMode(LED_BUILTIN, OUTPUT);

  HardwareScanner hw;
  hw.init(1, 127);
  hw.printFullDiagnostic(Serial);
  Serial << F("Reading from RF receiver and transfering to USB Serial...") << ENDL;
  Serial << F("Check: - RF board's TX (#13) to Uno's digital pin #") << RX_Pin << ENDL;
  Serial << F("       - No connection to Uno's TX (digital pin #") << TX_Pin << ENDL;
  Serial << F("       - Vcc & Ground connected") << ENDL;

  gtSerial.begin(serial1BaudRate);  // software serial port

  // Initializing SD Logger
  char result = logger.init("RFIN", CS_Pin);
  if (result < 0) {
    Serial << F("No SD card detected: data will not be stored") << ENDL;
    cardOK = false;
  } else {
    cardOK = true;
    Serial << F("Data will be stored in file '") << logger.fileName() << F("'") << ENDL;
  }

  // Conclusion of Init
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
  delay(500);
  digitalWrite(LED_BUILTIN, LOW);
  Serial << F("Ready to power up Isatis stack!") << ENDL;
  Serial << F("---------------------------------") << ENDL << ENDL;
}

void logAndEchoStream(Stream & str) {
  static byte idx = 0;
  byte c;
  while (str.available() > 0) {
    c = str.read();
    Serial.write(c);  // Echo on USB port.
    buffer[idx] = c;

    if ((c == '\n') || (idx == BufferSize - 2)) {
      digitalWrite(LED_BUILTIN, HIGH);
      buffer[idx + 1] = '\0';
       if (cardOK) {
        logger.log(buffer, false);
      } else
      {
        Serial << '*';
      }
      digitalWrite(LED_BUILTIN, LOW);
      idx = 0;
    } else {
      idx++;
    }
  } // while */
}

void loop() {
  logAndEchoStream(gtSerial);
}
