/*
 * Global constants & variables declaration.
 */
 
#include "DebugCSPU.h"
#include "IsatisConfig.h"

// ------------------ Constants ------------------
const unsigned int numReadings = 100;     // Number of readings to use for a single test (both with operational values or
                                         // for optimisation.
const bool ignoreFirstReading = true;
const bool useFan=false;     // True to have the fan operational during test. 

// ------------------ Variables ------------------
extern double minOutputV;
extern double maxOutputV;
extern double minDensity;
extern double maxDensity;
extern double totalOutputV;
extern double minOkOutputV;
extern double maxOkOutputV;
extern double minOkDensity;
extern double maxOkDensity;
extern double totalOkOutputV;
extern double totalOkDensity;

extern unsigned long minReadingDuration;
extern unsigned long maxReadingDuration;
extern long numOK_Readings;



