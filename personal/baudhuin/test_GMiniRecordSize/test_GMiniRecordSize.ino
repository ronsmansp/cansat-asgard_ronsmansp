#define CSPU_DEBUG
#include "DebugCSPU.h"
#include "GMiniRecordExample.h"
#include "CansatXBeeClient.h"

GMiniRecordExample exRecord;
GMiniRecord record;

constexpr uint8_t BufSize=80;
uint8_t exBuffer[BufSize], gmBuffer[BufSize];
uint8_t exBufferSize=BufSize, gmBufferSize=BufSize;

void setup() {
  DINIT(115200);
  record.clear();
  exRecord.clear();
  
  Serial << "binary sizes: " << record.getBinarySize() << " vs " << exRecord.getBinarySize() << ENDL;

  Serial << "Writing to binary:" << ENDL;
  if (!record.writeBinary(gmBuffer, gmBufferSize)) {
    Serial << "  GMiniRecord failed" << ENDL;
  }
   if (!exRecord.writeBinary(exBuffer, exBufferSize)) {
    Serial << "  GMiniRecordExample failed" << ENDL;
  }
  
  Serial << "Rereading:" << ENDL;
  if (!record.readBinary(gmBuffer, gmBufferSize)) {
    Serial << "  GMiniRecord failed" << ENDL;
  }
   if (!exRecord.readBinary(exBuffer, exBufferSize)) {
    Serial << "  GMiniRecordExample failed" << ENDL;
  }
   if (!exRecord.readBinary(gmBuffer, gmBufferSize)) {
    Serial << "  GMiniRecord into GMiniRecordExamplefailed" << ENDL;
  }
   if (!record.readBinary(exBuffer, exBufferSize)) {
    Serial << "  GMiniRecordExample into GMiniRecordExample failed" << ENDL;
  }
  Serial << "End of job" << ENDL; 
  exit(0);
}

void loop() {
  // put your main code here, to run repeatedly:

}
