
#pragma once

#define DEBUG_CSPU
#include "DebugCSPU.h"
#define __ASSERT_USE_STDERR // Required for our custom __assert() function to be called.
#include "assert.h"

#define SLOW_DOWN 0   // Define to 1 in add 1 sec delays. 
