CansatRecord: OK
CansatConfig.h: OK
BMP280_Client: OK, testé
ThermistorClient Relu
   Renommé les classes qui implémentent SteinhartHart. 
   test_ThermistorSteinhartHart: relu, compile, A FAIRE TOURNER. 
   Créé un ThermistorClient qui gère 1 à 3 thermistors. Test écrit pour les 3 thermistors, SW arch à jour. 
GPS_Client: 
	Créé la classe GPS_Client dans cansatAsgard.
	Créé le programme de test. 
	Compilation OK. Test OK.
	Pour le timer: oui, il est crée même quand GPS_ECHO est false, et c'est NORMAL et indispensable. Documenté dans SW arch.
HardwareScanner: Rien à modifier.
XBeeClient
   Repris l'IsaTwoXBeeClient comme CansatXBeeClient. 
      les constantes protégées devraient être dans la superclasse FAIT et dans la sous-classe (PAS SÛR?), on ne doit spécialiser
      que pour le display des records dans send et receive.
      créé le display frame dans XBeeClient_Utilities (à nettoyer).  Utiliser un displayCansatPayload dans la sous-classe. 
      Le display doit couvrir tous les frame types.
      Fait, compilation OK.
      Test_XBeeClient-Send/Receive : Compile, test ok.
      Test_CansatXBeeClient-Receive: Compile, test ok.
      Test_CansatXBee-halfduplex: Compile, test ok.       	
RT_CanCommander :
	- Repris le RT_Commander comme RT_CanCommander.
	- API mode OK
	- Transparent mode OK 
	- Compilation sur Arduino à valider. OK.
	* MYSTERE A PERCER: RT_CanCommander 498: RF_Stream->XBeeClient::send((uint8_t*) buffer, (uint8_t) (read+3), 0);
 	  POURQUOI le XBeeClient:: est-il nécesaire ????
HardwareScanner: OK
	- Repris tout qui était utile dans IsaTwoHW_Scanner
	- Splitté en 2 classes
	- Dédoublé le sketch de utils.
	- Mis à niveau le test unitaire 
	- Testé sur Uno et Feather.  
	- Documenté dans l'architecture SW.
StorageMgr: OK
	- Utiliser le CansatRecord  FAIT
	- Désactiver les EEPROM si IGNORE_EEPROMS est défini. FAIT
	- Test OK. 
AcquisitionProcess
  - Créé LED_Type.h
  - Scope de AcquisitionProcess et CansatAcqProcess clarifié. OK
  - Test: OK. 
  - Intégré tout ce qu'il y a dans IsaTwo (générique)en un CansatAcquisitionProcess
  - Gérer dans l'acq. process un HardwareScanner et dans le CSTprocess un CansatHW_Scanner. FAIT.
  - remplacer les méthodes purement virtuelles par des méthodes vides:  FAIT.
  - Remplacer AcqOn AcqOff par une enum class dans LED_Type.h FAIT
  - Test de CansatAcquisitionProcess avec le main. 
MainWithRT_Commander:
  - OK.
SD_Logger: 
    - Utiliser le CansatRecord FAIT
    - OK.
RF_Transceiver:
    - Fait une version générique dans Templates.
	- Problème avec les LEDs uplink et hearbeat: arrangé (soudure cassée).
	- Toutes commandes testées. 
Review documentation cansatAsgard: OK
Docs à mettre à jour: 
  * System architecture (1200). Fait.
RF_Transceiver: prévoir deux modes: un ou l'output RT-Commander est brut, un où il ne l'est pas. FAIT!
Créer le TorusCanCommander pour les commandes de parachute.

Créer une classe CansatRecordTestFlow, qui fournit des records depuis un fichier csv.
   - données IsaTwo elsenborn disponibles (test_CansatRecordTestFlow/TestData.
      Test ok. Fusionné. 
Compléter le TorusRecord les données de controleur + créé le TorusInterface.h: OK, fusionné.
      
Complété le CansatRecord avec l'altitude de référence. TEST A TERMINER cf. plus bas. 
Support BMP388: OK
Configuration de l'analogReference: OK? 

========= IN PROGRESS ===========
	
Probleme des records séparés de plus de 70 msec.   
* performMaintenance n'est pas implémenté dans SD_Logger. C'est normal:
  pour le moment on ouvre et ferme le fichier à chaque écriture, ce qui 
  fait un flush/sync chaque fois. 
* Analyse du timing: l'écriture SD ne prend jamais plus de 16 msec, même quand
  le cycle dépasse les 70... Le loop() ne prend jamais plus de 31 msec ????? 
* Conclusion: le temps est passé dans des interruptions ou des tâches système non
  contrôlées par nous. Pour améliorer l'écriture SD (flux rapide et ininterrompu)
  il faut utiliser un système complexe de multiples buffers et contrôler exactement
  ce qui se passe. A voir pour l'été ? 
Reprendre le calcul de l'altitude de référence du TorusSecondaryMissionController, et
l'intégrer dans le BMP_Client. 
	* Branche créée.
	* Supprimé de SecondaryMissionController, intégré dans BMP.
	* Settings transférés de TorusConfig.h à CansatConfig.h
	* Compilation TorusMain, testBMP et testSecondaryMissionController OK. 
	* New test program prepared and tested.
	* Compilation ok with and without INCLUDE_REFERENCE_ALTITUDE defined in CansatConfig.h 

Test Arduino IDE v2.0beta10
    * Fonctionne, mais les cartes installées n'apparaissent pas dans le menu?  Probablement 
      un bug de la bêta. 
CansatMain: reprendre les améliorations pertinentes faites dans TorusMain. OK
			Faire du main un template ? Non
RF_Transceiver: reprendre les améliorations pertinentes faites dans TorusRF-Transceiver. OK
			En faire un template ? Non
			

Arduino-cli:
    * Je compile un programme, dans le répertoire,  avec 
      arduino-cli compile --fqbn adafruit:samd:adafruit_feather_m0_express --warnings all
      (alias: clic). On peut ajouter --clean pour une compilation à blanc et/ou -v (--verbose)
      
========= IN PROGRESS ==============
 
 
 A tester avec ItsyBitsy M4:  
 		Serial2 OK.
 		Timers: 
 		  Compilation de TC_Timers flanche: a été désactivé pour SAMD51. 
		   M4 (SAMD51) ne fonctionne pas de la même manière que M0 (SAMD21) pour les timers
		   Librairie SAMD_InterruptTimers installée et testée avec M0 et M4 (exemple ISR_TimerArray): OK
		   Créer un test explicite pour bien comprendre (test_ISR_TimersM0-M4) FAIT test_SAMD_InterruptTimers.
		   Une fois qu'on a un usage portable,  OK
		   		* améliorer le programme de test OK
		   		* déplacer vers CansatAsgard. OK
 		HW_Scanner OK.
 		CansatHW_Scanner, OK.
 		GPS  	* Tester le nouveau  GPS_Client (PREPARE, #define NEW_VERSION)
		   		* si OK sortir TC_Timer de la librairie. TODO.
 		ADC   Test in utils. M4 chip has a silicon bug. AREF tied to 3.3V, cannot be used. 
 			  Documented. Question asked to support. 
 		I2C?  A tester pour être sûr, avec un main torus, sur M4 pour s'assurer que
 			  Serial2 et I2C n'interfèrent pas de manière imprévue. 
 		Servo: KO,  La lib servo 1.1.6 utilisée par Torus ne supporte pas SAMD51. Upgrade à la v1.1.8: toujours pas de support: 
 			Désactivé les classes AsyncServoWinch. Utiliser celle d'adafruit: https://github.com/adafruit/?q=servo 
 			Mais fonctionne-t-elle sur les cartes non-adafruit?
 		
   Tous les utilitaires (utils) à recompiler.  (ne devrait pas compiler TC_Timer...) 
   
  RF-Transceiver GMini: ajusté le feedback utilisateur. Codé. A tester avec 2 subs (fusionné)
  *** Le sub1 semble bien fonctionner mais le ground perd beaucoup trop de records.
      Tester avec une période plus grande ? Trouver la cause avant d'ajouter 
      la canette principale, et potentiellement un deuxième sub... 
        - le sub envoie bien tous les records. 
        - Le problème est-il l'écriture? Non. 
        - Le RF-Transceiver vérifier la RF in toutes les 1 msec. 
        - Le timing ne fonctionne pas: on recoit un rc 350 msec + vieux aprèsmoins de 200 msec?? 
          Je reçois 2 records distants de 140 msec à 30 msec d'écart??? La sous-can 
          émet trop vite???  
          	* Non, elle émet au bon rythme et vers les 2 bonnes adresses. 
        - Tout fonctionne bien lorsque la sub n'émet QUE vers le sol. Delay entre les émissions?
          5 ms ne change rien. 30 non plus, et même avec 100 msec, on perd des
          records.... L'émission prend 5 ms hors tout.
           Comme cela fonctionnait-il avec le simulateur???   En fait, cela ne 
           fonctionne pas ou plus??? Le ground simulator perd plein de records... en double 
           transmission, alors qu'il ne perd rien si on supprime l'émission vers
           la main can (ligne 14 dans le subCan_Simulator. 
        TROUVE: le problème se pose quand la XBee de la main can n'est pas en ligne. 
        Une des deux xbees doit essayer de la trouver et cela provoque des pertes. 
        C'était déjà documenté dans 1200/RF_CommunicationStrategy :-(
        
          
  
========= NOT DONE ============== 
	
Vérifier carte SD avec version plus récente de Adafruit SAMD. 

MAIN:  EN API, il ne faut pas envoyer le 1, initial, en transparent bien. A DOCUMENTER DANS L'ARCH. 

========= A FAIRE APRES CLOTURE DU PROJET, POUR LES SUIVANTS ==========


Faire une classe de base SecondaryMissionController? OK, test OK.
  * Attention, c'est le CansatAcquisition process qui envoie le record, donc 
    faut gérer la mission secondaire depuis CansatAcquisitionProcess::acquireDataRecord() (entre acquireSecondaryMissionData()
    et l'émission/stockage). OK
     * Avoir une méthode qui renvoie le SecondaryMissionController, comme pour le 
       HW_Scanner?  ou un getSecondaryMissionController qui renvoie un pointeur, et 
       appeler le begin et le run(). OUI. Fait Ok
  * Conséquence: la période d'acquisition contraint la période de gestion (qui doit être un multiple ou identique).
    (identique suffit sans doute: le SecondaryMissionCtrl peut rallentir si utile). OK, doc dans CansatConfig OK
  * Documenter dans la strategie de subclassage (doc?) Ok dans le header de SecondaryMC, TODO dans SW-Arch
  * Il faut annuler des changements dans le main. OK.
  * Main adapté à tester OK  
  * Lien avec le xxxRT_Commander? Il devra avoir accès à GMiniSecondaryMissionCtrl::lock/unlock/neutral.
    C'est le GMiniAcquisitionProcess qui devra fournir un getSecondaryMissionController(). A prévoir en standard? 
    OUI: le RT_CanCommander a un pointer sur l'acqProcess. Par là, il aura accès à tout.  OK 
  * Compléter l'arch software. OK
  
  
  
  * Documenter la réutilisation du RF_Transceiver + RT_Processing, + javascript back-end.   
  * Gérer la "LED multicolore" des ItsyBitsy? 
    
  
  	  
Reprendre l'ownership de torus_rt_processing
Reprendre l'ownership du back-end Javascript.

RF-Transceiver GUI: generaliser? 

Vérifier procédure GoogleEarth et back end. 
				

 
      

