/*
   EEPROMBankWriter.h  vBDH

   Current limitations:
     Only support same-size EEPROM chips.
     Cannot differentiate between 2 chips at 80-81 and 2 chips at 80-82.
        * If EEPROM is free from second chip, no problem.
        * If EEPROM has a partly full second chip: it will fill another one instead of the 
          now missing one: no issue.
     The only issue is for the reader, which could read in a chip that was partly not written.
     This is acceptable. 

*/

#pragma once

#include "Arduino.h"
#include "HardwareScanner.h"
#include "elapsedMillis.h"
#include "ExtEEPROM.h"
#include "VirtualMethods.h"

class EEPROM_BankWriter {
  public:
    typedef unsigned int EEPROM_Key;
    // This structure is written at address 0x00
    // The EEPROM is consequently empty when EEPROM_Header.firstFreeByte is at
    // sizeof(EEPROM_Header) and EEPROM_Header.firstFreeChip is 0.
    typedef struct EEPROM_Header {
      EEPROM_Key headerKey;	   // A constant to detect presence of a header. 
                               // Value is defined by the object owner.
      byte numChips; 	         // min = 1. Limited to 8 by IC2 addressing.
      EEPROM_Address chipLastAddress;  // The highest valid address within a chip (all chips are assumed to have the
                                       // same size.
      byte recordSize;         // 0 if memory empty or memory not empty or variable-size records are used.
      byte firstFreeChip;      // The sequence number of the chip in which the first free byte is (number from 0). 
                               // chip 0 is the one containing the header, possibly not the first available chip.
                               // Compared to HW scanner numbering, this numbering is offset by firstEEPROM_Chip. 
                               // If full, value is numChips.
      EEPROM_Address firstFreeByte;  // The address of the first free byte in the first free chip.
    } EEPROM_Header;

    EEPROM_BankWriter(const byte theMaintenancePeriodInSec = 10);
    VIRTUAL ~EEPROM_BankWriter();

    /* Initialize everything according to available hardware.
       Assumes hw.getNumExternalEEPROM() > 0. */
    VIRTUAL bool init(const EEPROM_Key theKey, const HardwareScanner &hw, const byte recordSize);
    // Method to call regularly in loop to allow for maintenance.
    VIRTUAL_FOR_TEST void doIdle();

    // Information methods
    NON_VIRTUAL unsigned long getTotalSize() const;
    NON_VIRTUAL unsigned long getFreeSpace() const;
    NON_VIRTUAL inline unsigned long getNumFreeRecords() const {
      return getFreeSpace() / header.recordSize;
    }
    NON_VIRTUAL inline bool isInitialized() const {
      return flags.initialized;
    }
    NON_VIRTUAL inline bool memoryFull() const {
      return flags.memoryFull;
    }

    // Write methods
    NON_VIRTUAL bool storeOneRecord(const byte* data, const byte dataSize);
    NON_VIRTUAL bool storeData(const byte* data, const byte dataSize);
    // Completely empty the EEPROM bank (logically). If any chip is not used
    // it is used after a call to erase().
	  NON_VIRTUAL void erase();
    // Empty the EEPROM bank from the byte located at addressInChip in the 
    // chip numbered chipId (counted from 0, 0 being the first chip in use.
	  NON_VIRTUAL void eraseFrom(byte chipId, EEPROM_Address addressInChip);

  protected:
    typedef struct Flags {
      byte initialized: 1;
      byte headerToBeUpdated: 1;
      byte memoryFull: 1;
    } Flags;

    // Methods for use by the subclasses.
    NON_VIRTUAL inline const EEPROM_Header& getHeader() const {return header;}
    NON_VIRTUAL inline const HardwareScanner* getHardware() const { return hardware;}
    /* Read data. 32 bytes is the max size allowed by underlying Wire library */
    VIRTUAL_FOR_TEST unsigned int readFromEEPROM(const byte EEPROM_I2C_Address, EEPROM_Address address, byte * buffer, const byte size) const;
    VIRTUAL_FOR_TEST unsigned int writeToEEPROM(const byte EEPROM_I2C_Address, EEPROM_Address address, const  byte * buffer,  const byte size);
    NON_VIRTUAL byte nextChip(const byte current);
    NON_VIRTUAL inline byte getFirstEEPROM_Chip() const { return firstEEPROM_Chip;}
    NON_VIRTUAL inline const Flags& getFlags() const { return flags;}

  private:
    /* Read header from EEPROM (if any),
       check internal consistency and consistency with hardware & internal
       Return value: true if succesfull, false otherwise.
    */
    VIRTUAL_FOR_TEST bool initHeaderInfo(
      const EEPROM_Key headerKey, const HardwareScanner &hw, byte recordSize);
    NON_VIRTUAL bool goToNextChip();
    NON_VIRTUAL bool noMoreChip() const;

    /* Read the header at the bottom of chip at EEPROM_I2C_Address */
    VIRTUAL_FOR_TEST void readHeader(const byte EEPROM_I2C_Address);
    /* Write the header at the bottom of chip at EEPROM_I2C_Address */
    VIRTUAL_FOR_TEST void writeHeader(const byte EEPROM_I2C_Address);

    const HardwareScanner * hardware;
    byte firstEEPROM_Chip;  // The sequence number of the first chip used (as reported by the HW Scanner).It might not be
    						            // the chip #0, if it does not contain a valid header and a next chip does
    						            // (e.g.: 3 chips 0-1-2, header with numChips = 2 in chip 1.
                            // The first free chip is consequently at firstEEPROM_Chip+header.firstFreeChip.
    EEPROM_Header header;
    Flags flags;			  // The various flags to maintain the state of the EEPROM_Bank
    elapsedMillis timeElapsedSinceMaintenance;
    byte maintenancePeriodInSec; // The period (in seconds) used for checking the need for header update.
};


