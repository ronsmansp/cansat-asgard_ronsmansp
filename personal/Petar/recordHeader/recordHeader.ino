#define DEBUG_CSPU
#include "DebugCSPU.h"

#include "TorusRecord.h"

void setup() {
  DINIT(115200);
  
  TorusRecord record;
  record.printCSV(Serial, TorusRecord::DataSelector::All, TorusRecord::HeaderOrContent::Header);
}

void loop() {
  // put your main code here, to run repeatedly:

}
