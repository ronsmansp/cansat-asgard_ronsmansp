#include <SPI.h>
#include "SdFat.h"
#define DEBUG_CSPU 1
#include "DebugCSPU.h"

SdFat sd;
#define SD_CS_PIN SS
File file;

char* fileName = " ";

void setup() {
  Serial.begin(115200);
  while (!Serial) {
    ;
  }
  Serial << "Initializing SD Card" << ENDL;
  if (!sd.begin(SD_CS_PIN)) {
    Serial << "Error Initializing SD card" << ENDL;
  }
  Serial << "Initializaion Done" << ENDL;
  Serial << "**FILES IN ROOT DIRECTORY**" << ENDL;
  sd.ls("/", LS_SIZE);
  Serial << "Input the file you want to open ([File Name].[Extension], Replace spaces by ""~"" )" << ENDL;

  while (Serial.available() > 0) {
    Serial << "EE" << ENDL;
    Serial << "EEE" << ENDL;
    fileName = Serial.read();

    Serial << "EEEE" << ENDL;
    file = sd.open(fileName, FILE_READ);
    if (file) {
      Serial.println("File:");
      while (file.available()) {
        Serial.write(file.read());
      }
      file.close();
    } else {
      Serial.println("error opening File");
    }
  }
  Serial << "EEEEE" << ENDL;
}
void loop() {}
