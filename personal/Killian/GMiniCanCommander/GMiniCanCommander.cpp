/*
   TorusCanCommander.cpp
*/
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#define DEBUG_CSPU
#include "DebugCSPU.h"
#include "CansatInterface.h"
#include "GMiniCanCommander.h"

#define DBG_DIAGNOSTIC 1

GMiniCanCommander::GMiniCanCommander(unsigned long int theTimeOut) :
  RT_CanCommander(theTimeOut), servo(NULL) {
}

#ifdef RF_ACTIVATE_API_MODE
void GMiniCanCommander::begin(CansatXBeeClient &xbeeClient, SdFat* theSd,
                              AcquisitionProcess* theProcess, ServoLatch* myServo) {
  RT_CanCommander::begin(xbeeClient, theSd, theProcess);
  servo = myServo;
}
#else
void GMiniCanCommander::begin (Stream& theResponseStream, SdFat* theSd, AcquisitionProcess* theProcess, ServoLatch* Myservo) {
  RT_CanCommander::begin(theResponseStream, theSd, theProcess);
 servo = myServo;
}
#endif

bool GMiniCanCommander::processProjectCommand(CansatCmdRequestType requestType,
    char* cmd) {
  bool result = true;

  switch (requestType) {
    case CansatCmdRequestType::Get_ServoLatch_LockStatus:
      processReq_Get_ServoLatch_LockStatus(cmd);
      break;

    case CansatCmdRequestType::ServoLatch_Lock:
      processReq_ServoLatch_Lock(cmd);
      break;

    case CansatCmdRequestType::ServoLatch_Unlock:
      processReq_ServoLatch_Unlock(cmd);
      break;

    default:
      // Do not report error here: this is done by the superclass.
      result = false;
  } // Switch
  return result;
}
