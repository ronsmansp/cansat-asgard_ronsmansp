 #include "SOS.h"
bool SOS::begin(const uint8_t ledPinNbr) {
  pinMode (ledPinNbr, OUTPUT);
  pinNbr = ledPinNbr;
  return true;
}
void SOS::flash(int duration) {
  digitalWrite(pinNbr, HIGH);
  delay( duration );
  digitalWrite(pinNbr, LOW);
  delay( duration );
}

void SOS::writeS() {
  for (int i = 0; i < 3; i++) {
    flash (200);
  }
}
void SOS::writeO() {
  for (int i = 0; i < 3; i++) {
    flash(500);
  }
}


void SOS::blink() {
  for (int i = 0; i < 3; i++) {
    if (i == 0 || i == 2) {
      writeS();
    }
    else if (i == 1) {
      writeO();
    }
  }
}
