#define DEBUG_CSPU
#include "DebugCSPU.h"
#include "ServoControl.h"

ServoDetach srvDet;

void setup() {
  Serial.begin (115200);
  srvDet.begin (9);
}

void loop() {
  srvDet.unlock();
  delay(3000);
  srvDet.lock();
  delay(3000);
}
