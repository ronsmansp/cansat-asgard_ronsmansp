/**
 * Receiving funcion only from the XBeeClient.h
 */


#include <XBee.h>
#define DEBUG_CSPU
#include "DebugCSPU.h"
#define DBG_RECEIVE 1

HardwareSerial &RF = Serial1;
XBee xbee = XBee();

uint8_t* payload;
uint8_t payloadSize;

bool receive(uint8_t* &payload, uint8_t& payloadSize) {                                 //POURQUOI ????
  bool result = false;
  XBeeResponse response = XBeeResponse();           //Only used here so okay?
  ZBRxResponse rx = ZBRxResponse();                 //Only used here so okay?
  ModemStatusResponse msr = ModemStatusResponse();  //Only used here so okay?
  xbee.readPacket(); //Checks the communication at every loop
  if (xbee.getResponse().isAvailable()) {
    DPRINTS(DBG_RECEIVE, "We got something: ");
    uint8_t apiId = xbee.getResponse().getApiId();
    if (apiId == ZB_RX_RESPONSE) {
      DPRINTSLN(DBG_RECEIVE, "a zb rx packet!");
      //Now fill our zb rx class
      xbee.getResponse().getZBRxResponse(rx);
      uint8_t resp = rx.getOption();
      if (resp == ZB_PACKET_ACKNOWLEDGED) {
        // the sender got an ACK
        DPRINTSLN(DBG_RECEIVE, "Sender got en ACK - good");
        DPRINTS(DBG_RECEIVE, "ZBRxResponse: ");
        DPRINTLN(DBG_RECEIVE, resp);
        payload = rx.getData();
        payloadSize = rx.getDataLength();
        for (int i = 0; i <= payloadSize; i++) {
          Serial << "Bytes received #" << i << payload[i] << ENDL;
        }
        result = true;
      } else {
        //***ERROR***
        DPRINTSLN(DBG_RECEIVE, "***ERROR*** We received the packet but the sender didn't get an ACK - Problem");
        DPRINTS(DBG_RECEIVE, "API ID: ");
        DPRINTLN(DBG_RECEIVE, apiId);
        DPRINTS(DBG_RECEIVE, "ZBRxResponse: ");
        DPRINTLN(DBG_RECEIVE, resp);
        result = false;
      }
    } else if (xbee.getResponse().getApiId() == MODEM_STATUS_RESPONSE) {
      xbee.getResponse().getModemStatusResponse(msr);
      // the local XBee sends this response on certain events, like association/dissociation
      DPRINTS(DBG_RECEIVE, "A certain event happened association/dissociation: ");
      if (msr.getStatus() == ASSOCIATED) {
        DPRINTSLN(DBG_RECEIVE, "It's an assiciation - Yeay this is great!");
        result = true;
      } else if (msr.getStatus() == DISASSOCIATED) {
        //***ERROR***
        DPRINTSLN(DBG_RECEIVE, "It's a dissociation - Aïe this is awful");                      //Could be a switch ?
        result = false;
      } else {
        //***ERROR**
        DPRINTSLN(DBG_RECEIVE, "We received another (unknown?) status");
        result = false;
      }
    } else {
      //***ERROR***
      DPRINTSLN(DBG_RECEIVE, "We received somehing unexpected...");
      result = false;
    }
  } else if (xbee.getResponse().isError()) {
    //***ERROR***
    DPRINTSLN(DBG_RECEIVE, "***ERROR*** in reading packet");
    DPRINTS(DBG_RECEIVE, "Error Code: ");
    uint8_t numError = xbee.getResponse().getErrorCode();
    DPRINTLN(DBG_RECEIVE, numError);
    result = false;
  } else {
  }
  return result;
}
void setup() {
  Serial.begin(115200);
  while (!Serial) {
    ;
  }
  RF.begin(115200);
  xbee.begin(RF);
  Serial << "---Beginning of the Tests---" << ENDL;
}

void loop() {
  bool result = receive(payload, payloadSize);
  if (result) {
    for (int i = 0 ; i < payloadSize; i++) {
      Serial << "  Byte #" << i << ": " << payload[i] << "-";
    }
  }
}
