#include "IsatisDataRecord.h"
#include "elapsedMillis.h"
IsatisDataRecord record;
elapsedMillis elapsed;

void setup() {
  // put your setup code here, to run once:
DINIT(115200);

}

void loop() {
  // put your main code here, to run repeatedly:
  elapsed=0;
  Serial << record.BMP_Temperature << ENDL;
  Serial << record.BMP_Pressure << ENDL;
  Serial <<record.BMP_Altitude << ENDL;
  delay(1000);
}
