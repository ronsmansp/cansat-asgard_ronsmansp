#include "blinkingled.h"

blinkingled led(12, 1000);
blinkingled led2(10, 333);

void setup() {
  // put your setup code here, to run once:
  led.setup();
  led2.setup();
}

void loop() {
  // put your main code here, to run repeatedly:
  led.run();
  led2.run();
}
