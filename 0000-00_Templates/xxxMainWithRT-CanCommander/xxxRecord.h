/*
 * xxxRecord.h
 */

#pragma once
#include "CansatRecord.h"

/** @brief A CansatRecord for an hypothetical xxx project.
 *  This dummy class demonstrate the architecture of a Cansat project
 *  in conjunction with xxxMainWithRT-CanCommander sketch. 
 *  NB: The implementation is not complete! See CansatRecord documentation for details. 
 */
class xxxRecord: public CansatRecord {
	// Add here the project specific data
public:
	int myXxxSpecificData;
	// Implement those methods to support secondary mission data.
	// See examples in CansatRecordExample, TorusRecord, GMiniRecord etc.
	/*
	virtual void printCSV_SecondaryMissionData(Stream & str,
			bool startWithSeparator, bool finalSeparator) const ;

	virtual void printCSV_SecondaryMissionHeader(Stream & str,
			bool startWithSeparator, bool finalSeparator) const ;

	virtual void clearSecondaryMissionData() ;

	virtual uint16_t getSecondaryMissionMaxCSV_Size() const ;

	virtual uint16_t getSecondaryMissionCSV_HeaderSize() const ;

	virtual void printSecondaryMissionData(Stream& str) const ;

    virtual uint8_t writeBinarySecondaryMissionData(uint8_t* const destinationBuffer, uint8_t bufferSize) const ;

    virtual uint8_t readBinarySecondaryMissionData(const uint8_t* const sourceBuffer, const uint8_t bufferSize);

    virtual uint8_t getBinarySizeSecondaryMissionData() const;
    */

};
