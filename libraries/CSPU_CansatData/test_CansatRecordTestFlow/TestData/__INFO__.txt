About the test files:

- IsaTwoElsenbornOriginalComplete.csv is the row file from the IsaTwo file in Elsenborn, in 2019.
- Isa2Full.csv
	This is the raw data collected during IsaTwo flight in Elsenborn
	  - limited to the primary mission + GPS data
      - augmented with empty columns for all now additional columns of the CansatRecord 
        class.
	  - flight starts at line 179736 (timestamp 12 841 180 msec, i.e 3.55h after start-up)
- Isa2Full.csv
	This is the data collected during IsaTwo flight in Elsenborn
	  - limited to the primary mission + GPS data
      - augmented with empty columns for all now additional columns of the CansatRecord 
        class.
	  - flight starts at line 179736 (timestamp 12 841 180 msec, i.e 3.55h after start-up)
- Isa2_10m.csv
	This is the data collected during IsaTwo flight in Elsenborn
	  - limited to the primary mission + GPS data
      - augmented with empty columns for all now additional columns of the CansatRecord 
        class.
	  - with only 10 min before flight (flight starts at line 8440, timestamp 12 841 180 msec,
	    i.e 3.55h after start-up)
	  - with less than 10 min after flight (flight ends at line 10500, timestamp 12 985 696 msec,
	    i.e 144.5 sec after take-off)
- I2_10mDV.ods 
  	 This is the same as Isa2_10m.csv completed with DescentVelocityCalculation
- I2_10mDV.csv
	 This is the same as I2_10mDV.ods in CSV format, without the columns performing the 
	 descent velocity calculation (but with the results in the descentVelocity column). 