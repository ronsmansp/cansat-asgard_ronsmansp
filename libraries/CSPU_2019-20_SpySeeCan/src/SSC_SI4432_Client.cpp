#define DEBUG_CSPU
#include "DebugCSPU.h"
#include "SSC_SI4432_Client.h"
#include "SSC_Config.h"
#define DBG_READ_DATA 1

SSC_SI4432_Client::SSC_SI4432_Client(uint8_t SI4432_CS) :  SI4432_Client(SI4432_CS) {
	cycleIndex=0;
}

bool SSC_SI4432_Client::begin()
{
   return SI4432_Client::begin();
}

bool SSC_SI4432_Client::readData(SSC_Record &record)
{
    float freq = StartFrequency[cycleIndex];
    record.setStartFrequency(freq);

	DPRINTS(DBG_READ_DATA, "readData: cycleIdx=");
	DPRINT(DBG_READ_DATA, cycleIndex);
	DPRINTS(DBG_READ_DATA, ", startFreq=");
	DPRINT(DBG_READ_DATA, freq);
	DPRINTS(DBG_READ_DATA, "MHz, step=");
	DPRINTLN(DBG_READ_DATA, record.getStartFrequency());

    record.setFrequencyStep(FrequencyStep);
    for (unsigned int i = 0; i < SSC_Record::RF_POWER_TABLE_SIZE; i++)
    {
        record.setRF_Power(i, readPower(freq));
        freq+=FrequencyStep;
    }
    cycleIndex++;
    if (cycleIndex >= NumFrequencyBand) cycleIndex=0;
    return true;
}
