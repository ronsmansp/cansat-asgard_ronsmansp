#ifdef IGNORED_THIS_IS_TMP
/*
   SD_Logger.h

   A class providing the required SD Services to the StorageManager

   INFO TO BE MOVED IN DESIGN DOCUMENT. 
   The SD library cause some issues:
     - It includes an out-dated version of SdFat, and does not expose any method to access free space (confirmed in doc)
     - It is incompatible with a recent version of SdFat.
   On the other hand, recent versions of SdFat expose all interfaces of SD, AND methods to access free space AND have a 
   lower memory consumption (https://forum.arduino.cc/index.php?topic=274784.0).
   As a consequence, we do not use the SD library but the SdFat. 

   Initialisation of library: this initializes the SPI library transparently.
   
   Design options: 
   1. do we close the file after each write or just flush it ? Is there any problem removing
   the SD_Card with open & flushed files? Performance is excellent with both options (<30 msec / write, for about 60 bytes à data). 
   2. would it be better to flush or close/reopen in doIdle() rather than in log?  Implemented but no significant improvement. 

*/
#pragma once
#include <SdFat.h>
#include "elapsedMillis.h"

//#define STORE_SDFAT_OBJECT         // Define to have an instance of SdFat kept between calls. 

/** @ingroup cansatAsgardCSPU
 *  @brief A COMPLETER
 *  
 *  DESCRIPTION A COMPLETER.
 */
class SD_Logger {
  public:
    const unsigned long maintenancePeriod = 10000; // maintenance period in milliseconds. 
    SD_Logger();
    /*  Initialize the SD library, checks if the required amount of free space is available,
        define the name of the log file and creates it.
        The filename is composed as AAAAnnnn.txt where:
          AAAA is the first 4 characters of the fourCharPrefix converted to uppercases,
          nnnn is the smallest non null integer number (on 4 digits) such as AAAAnnnn.txt does not exist.
        If msg is not an empty string, it is written as first line of the file.
        The file is then closed.
        Return 0 in case of no error.
               -1: if initialization could not be performed: no card, card not formatted, card full, card read-only...
               1: if initialisation was succesfull but required free space is not available.
        Multiple inits should be supported (the last one overriding the previous one. */
    char init( const char * fourCharPrefix,
               const byte chipSelectPinNumber = 10,
               const String& msg = "",
               const unsigned int requiredFreeMegs = 1);

    /* Append data to the logfile, completed with a "\n" and flush the file */
    bool log(const String& data, const bool addFinalCR=true);

    /* Return the name of the log file, if any, or an empty string if none. */
    const String& fileName()  const ;

    /*  This method should be called regularly in the loop() processing, to allow for internal maintenance
        (flushing the file to avoid data loss, etc.). After a few seconds, it should always be safe to unplug
        the board and remove the card. 
        Returns true if maintenance was actually performed. */
    bool doIdle();

    /*  Return the size in bytes of the logfile in bytes (does not work for files larger than 2Gb due to
     *  unsigned long limitation.
     */
    unsigned long fileSize() ;

    /* Return the free space on the SD card. 
     * freeSpaceInBytes() returns UINT_MAX if the free space is larger than UINT_MAX bytes.
     */
    unsigned long freeSpaceInBytes() ;

  private:
    /* Return the free space on SD card. */ 
    unsigned long freeSpaceInMBytes(SdFat &SD) ;
    //Build a valid file name based on prefix and sequence number. 
    void buildFileName( const char* fourCharPrefix, const byte number);

    String logFileName;  // the name of the log file.
    
#ifdef STORE_SDFAT_OBJECT
    SdFat SD;
#else
    byte chipSelectPinNumber;
#endif

    elapsedMillis timeSinceMaintenance;
};

#endif
