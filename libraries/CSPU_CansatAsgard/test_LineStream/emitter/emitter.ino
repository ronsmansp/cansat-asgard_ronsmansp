/*
   Emitter sketch for testing the LineStream class.

   Emits on serial port Serial1.

   On an Arduino UNO: Rx = RxPinOnUno, TX = TxPinOnUno (see below)
   On a Feather board: uses hardware RX-TX

   NB: never transmit the \0 character (they are not transferred on the serial port!)

   Connect Rx and Tx to Tx and Rx of receiver board.
   Do not forget to connect the grounds!
*/
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

const unsigned long SerialBaudRate = 115200;
const unsigned int MinimumLength = 50;
const unsigned int BufferSize = 150;
const byte SizeIncrement=5;
constexpr byte RxPinOnUno = 9;
constexpr byte TxPinOnUno = 11;
constexpr byte MinRandomDelay = 5;
constexpr byte MaxRandomDelay = 30; // Delays in msec.

#include "DebugCSPU.h"

#ifndef ARDUINO_SAMD_FEATHER_M0_EXPRESS
#include "SoftwareSerial.h"
SoftwareSerial Serial1(RxPinOnUno, TxPinOnUno);
#endif

char buffer[BufferSize+1];
unsigned int counter = 0;

void setup() {
  DINIT(115200);
  Serial1.begin(SerialBaudRate);
  for (int i = 0; i < BufferSize; i++)
  {
    buffer[i] = 'a' + (i % 26);
  }
  buffer[BufferSize]='\0';
  randomSeed(analogRead(0));
  
  Serial << "Setup OK" << ENDL;
  Serial << "This program sends strings with lengths from 0 to "
         << BufferSize
         << " terminate by various combinations of end of string and end of line markers." << ENDL;
  Serial << "Each string should be received 9 times, identically" << ENDL;
  Serial << "Empty string should never be received" << ENDL;
#ifdef ARDUINO_SAMD_FEATHER_M0_EXPRESS
  Serial << "Using hardware serial port Serial1";
#else
  Serial << "RX = " << RxPinOnUno << ", TX=" << TxPinOnUno;
#endif
  Serial <<". Baudrate: " << SerialBaudRate << ENDL;
  Serial << "Content of buffer: " << buffer << ENDL;
  Serial << ENDL << ENDL;
}

void sendChars(byte numChars) {
  for (int i = 0; i < numChars; i++) {
    Serial1 << buffer[i];
    Serial << buffer[i]; // TMP
  }
  Serial << ENDL; // TMP
}
void loop() {
  Serial << "Sending " << counter << " characters..." << ENDL;
  sendChars(counter);
  Serial1 << '\n';
  delay(20);
  sendChars(counter);
  Serial1 << '\r';
  delay(20);
  sendChars(counter);
  Serial1 << "\n\r";
  delay(20);
  sendChars(counter);
  Serial1 << "\r\n";
  delay(20);
  sendChars(counter);
  Serial1 << "\n\n\n";
  delay(20);
  sendChars(counter);
  Serial1.println();
  delay(20);
  sendChars(counter);
  Serial1 << ENDL;
  delay(20);
  Serial1.println("---");
  counter+=SizeIncrement;
  if (counter > BufferSize)  {
    counter = MinimumLength;
  }
  delay(random(MinRandomDelay,MaxRandomDelay));
}
