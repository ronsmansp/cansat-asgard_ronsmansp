//ExtEEPROM 15/O2/2018
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#define DEBUG_CSPU
//#define USE_ASSERTIONS
#include "DebugCSPU.h"

#define DBG_WIRE_ERRORS 0
#define DBG_PAGE_WRITE 0
#define DBG_DIAGNOSTIC 0
#define DBG_READ_DATA 0
#include "ExtEEPROM.h"
#include <Wire.h>

bool ExtEEPROM::writeByte(const byte I2C_Address, const EEPROM_Address address, const byte data)
{
  bool result=false;
  Wire.beginTransmission(I2C_Address);
  Wire.write((int)(address >> 8));   // MSB
  Wire.write((int)(address & 0xFF)); // LSB
  Wire.write(data);
  byte resultByte = Wire.endTransmission();

  if (resultByte == 0) {
    delay(5);
    result= true;
  }
  else {
    DPRINTS(DBG_DIAGNOSTIC, "** writeByte: error ");
    DPRINTLN(DBG_DIAGNOSTIC, (int) resultByte);
  }
  return result;
}

bool ExtEEPROM::readByte( const byte I2C_Address, const EEPROM_Address address, byte &data )
{
  bool result = false;

  Wire.beginTransmission(I2C_Address);
  Wire.write((int)(address >> 8));
  Wire.write((int)(address & 0xFF));
  byte resultByte = Wire.endTransmission();

  if (resultByte == 0) {
    Wire.requestFrom((uint8_t) I2C_Address, (uint8_t)  1);
    delay(5);

    resultByte = Wire.available();
    if (resultByte == 1) {
      data = Wire.read();
      result = true;
    }
    else {
      DPRINTS(DBG_DIAGNOSTIC, "** readByte: found ");
      DPRINTLN(DBG_DIAGNOSTIC, (int) resultByte);
    }
  }
  else {
    DPRINTS(DBG_DIAGNOSTIC, "*** readByte: error ");
    DPRINTLN(DBG_DIAGNOSTIC, (int) resultByte);
  }
  return result;
}

byte ExtEEPROM::writeData(const byte I2C_Address, const EEPROM_Address startAddress, const byte * buffer, const byte bufferSize)
{
  // Uses Page Write for 24LC256. Allows for 64 byte page boundary, Splits data into max 16 byte writes
  // Code based on http://www.hobbytronics.co.uk/eeprom-page-write, generalised to support any type of data.
  unsigned char byteCounter = 0;
  unsigned int  address;
  unsigned int  page_space;
  unsigned int  page = 0;
  unsigned int  num_writes;
  unsigned char first_write_size;
  unsigned char last_write_size;
  unsigned char write_size;

  DPRINTS(DBG_PAGE_WRITE, "buffer size: ");
  DPRINTLN(DBG_PAGE_WRITE, bufferSize);

  // Calculate space available in first page
  page_space = int(((startAddress / 64) + 1) * 64) - startAddress;

  // Calculate first write size
  if (page_space > 16) {
    first_write_size = (page_space % 16);
    if (first_write_size == 0) first_write_size = 16;
  }
  else {
    first_write_size = page_space;
  }
  if (first_write_size > bufferSize) {
    first_write_size = bufferSize;  // Didn't work for dataSize < 16.
  }

  // calculate size of last write
  if (bufferSize > first_write_size)
    last_write_size = (bufferSize - first_write_size) % 16;
  else
    last_write_size = 0;

  // Calculate how many writes we need
  if (bufferSize > first_write_size)
    num_writes = ((bufferSize - first_write_size) / 16) + 2;
  else
    num_writes = 1;

  DPRINTS(DBG_PAGE_WRITE, "page space (bytes) : ");
  DPRINTLN(DBG_PAGE_WRITE, page_space);
  DPRINTS(DBG_PAGE_WRITE, "first write: ");
  DPRINT(DBG_PAGE_WRITE, first_write_size);
  DPRINTS(DBG_PAGE_WRITE, " bytes, last write: ");
  DPRINT(DBG_PAGE_WRITE, last_write_size);
  DPRINTS(DBG_PAGE_WRITE, " bytes, num writes: ");
  DPRINTLN(DBG_PAGE_WRITE, num_writes);

  byteCounter = 0;
  address = startAddress;
  for (page = 0; page < num_writes; page++)
  {
    if (page == 0) write_size = first_write_size;
    else if (page == (num_writes - 1)) write_size = last_write_size;
    else write_size = 16;

    Wire.beginTransmission(I2C_Address);
    Wire.write((int)((address) >> 8));   // MSB
    Wire.write((int)((address) & 0xFF)); // LSB
#ifndef USE_SINGLE_WRITE_FOR_BUFFER
    for (byte counter = 0; counter < write_size; counter++) {
      DASSERT(byteCounter < bufferSize);
      Wire.write((byte) buffer[byteCounter]);
      byteCounter++;
    }
#else
    DPRINTS(DBG_PAGE_WRITE, "byteCounter/write_size:");
    DPRINT(DBG_PAGE_WRITE, byteCounter);
    DPRINTS(DBG_PAGE_WRITE, "/");
    DPRINT(DBG_PAGE_WRITE, write_size);
    DPRINTS(DBG_PAGE_WRITE, " bufferSize: ");
    DPRINTLN(DBG_PAGE_WRITE, bufferSize);

    DASSERT(((byteCounter + write_size) <= bufferSize));
    Wire.write( &buffer[byteCounter], write_size);
    byteCounter += write_size;
#endif

    byte status = Wire.endTransmission();
    if (status != 0) {
      DPRINTS(DBG_WIRE_ERRORS, "** writeData: error:");
      DPRINTLN(DBG_WIRE_ERRORS, status);
      return byteCounter - write_size;
    }
    address += write_size; // Increment address for next write

    delay(6);  // needs 5ms for page write
  }
  return (address - startAddress);
}

byte ExtEEPROM::readData(const byte I2C_Address, const EEPROM_Address address, byte * buffer, const byte bufferSize)
{
  DPRINTS(DBG_READ_DATA, "readData, bufferSize=");
  DPRINT(DBG_READ_DATA, bufferSize);
  DPRINTS(DBG_READ_DATA, ", buffer at 0x");
  DPRINT(DBG_READ_DATA, (unsigned long) buffer, HEX);
  DPRINTS(DBG_READ_DATA, ", address=");
  DPRINTLN(DBG_READ_DATA, address, HEX);
  if (bufferSize == 0) return 0;

  byte read = 0;
  while (read < bufferSize) {
    byte chunkSize = bufferSize - read;
    if (chunkSize > 32) chunkSize = 32;
    byte result = readSmallData(I2C_Address, address + read, &buffer[read], chunkSize);
    // Possible error is documented in readSmallData()
    if (result != chunkSize) return read;
    else {
      read += result;
    }
  }
  return read;
}

byte ExtEEPROM::readSmallData(const byte I2C_Address, const EEPROM_Address address, byte * buffer, const byte bufferSize)
{
  DPRINTS(DBG_READ_DATA, "readSmallData, bufferSize=");
  DPRINT(DBG_READ_DATA, bufferSize);
  DPRINTS(DBG_READ_DATA, ", buffer at 0x");
  DPRINT(DBG_READ_DATA, (unsigned long) buffer, HEX);
  DPRINTS(DBG_READ_DATA, ", address=");
  DPRINTLN(DBG_READ_DATA, address, HEX);
  DASSERT(bufferSize <= 32)
  byte result = 0;
  Wire.beginTransmission(I2C_Address);
  Wire.write((int)(address >> 8));   // MSB
  Wire.write((int)(address & 0xFF)); // LSB
  byte resultData = Wire.endTransmission();

  if (resultData == 0) {
    Wire.requestFrom((uint8_t) I2C_Address, (uint8_t) bufferSize);
    delay(6);
    byte i = 0;
    while (Wire.available()) {
      buffer[i++] = Wire.read();
    }
    result = i;
  }

  else {
    DPRINTS(DBG_DIAGNOSTIC, "** readSmallData: error ");
    DPRINTLN(DBG_DIAGNOSTIC, (int) resultData);
  }
  return result;
}

