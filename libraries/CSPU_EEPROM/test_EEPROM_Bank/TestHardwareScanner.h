/*
 * A Fake harware scanner which always reports 0 EEPROM chip
 */

 #pragma once
 #include "HardwareScanner.h"


 class TestHardwareScanner : public HardwareScanner {
  virtual byte  getNumExternalEEPROM( ) const ;
 };

