/*
 * IMU_NXP_CalibrationData.h
 *
 */

#pragma once
#include "IsaTwoConfig.h"

/** The structure containing all calibration parameters for the NXP IMU */
#if (IMU_REFERENCE==NXP_A)
// Version from 30/3 with MotionCal for NXPB
const struct IMU_Calib {
  float accelZeroG[3] =  {108.45996093f,  -134.9399414f,  135.76013183f};
  float accelResolution[3] =  {0.00237104f, 0.00243699f,  0.0023924f};
  float gyroZeroRate[3] =  {14.76000022f,-9.64000034f,-4.53999996f};
  float gyroResolution[3] =  {0.0078125f,0.0078125f,0.0078125f};
  float magOffset[3] = { 73.30f,52.65f,95.05f};
  float magTransformationMatrix[3][3]= {
        { 0.977f , -0.025f, -0.009f},
          { -0.025f, 0.975f , -0.011f },
          {-0.009f, -0.011f, 1.051f } };

} IMU_Calib;

#elif (IMU_REFERENCE==NXP_B)
// Version from 25/3 with MotionCal for NXPB
const struct IMU_Calib {
	float accelZeroG[3] =  {108.45996093f,	-134.9399414f,	135.76013183f};
	float accelResolution[3] =  {0.00237104f,	0.00243699f,	0.0023924f};
	float gyroZeroRate[3] =  {14.76000022f,-9.64000034f,-4.53999996f};
	float gyroResolution[3] =  {0.0078125f,0.0078125f,0.0078125f};
	float magOffset[3] = { 50.60f,67.49f,70.77f};
	float magTransformationMatrix[3][3]= {
			  { 0.987f , -0.036f, 0.015f},
		      { -0.036f, 0.983f , 0.004f },
		      {-0.015f, 0.004f, 1.033f } };

} IMU_Calib;
#else
#  error "Unexpected IMU_REFERENCE in IMU_NXP_CalibrationData.h"
#endif
