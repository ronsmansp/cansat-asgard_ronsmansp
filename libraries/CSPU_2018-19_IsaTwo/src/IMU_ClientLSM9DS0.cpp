/*
   IMU_ClientLSM9DS0.cpp
*/

#include "IMU_ClientLSM9DS0.h"

#include "DebugCSPU.h"
#define DBG_LSM_READ_DATA 0

IMU_ClientLSM9DS0::IMU_ClientLSM9DS0(byte nSamplesPerRead): IMU_Client(nSamplesPerRead), lsm() {}

bool IMU_ClientLSM9DS0::begin() {
  if (!lsm.begin()) return false;

  // configure lsm
  // 1.) Set the accelerometer range
  lsm.setupAccel(lsm.LSM9DS0_ACCELRANGE_2G);
  //lsm.setupAccel(lsm.LSM9DS0_ACCELRANGE_4G);
  //lsm.setupAccel(lsm.LSM9DS0_ACCELRANGE_6G);
  //lsm.setupAccel(lsm.LSM9DS0_ACCELRANGE_8G);
  //lsm.setupAccel(lsm.LSM9DS0_ACCELRANGE_16G);

  // 2.) Set the magnetometer sensitivity
  lsm.setupMag(lsm.LSM9DS0_MAGGAIN_2GAUSS);
  //lsm.setupMag(lsm.LSM9DS0_MAGGAIN_4GAUSS);
  //lsm.setupMag(lsm.LSM9DS0_MAGGAIN_8GAUSS);
  //lsm.setupMag(lsm.LSM9DS0_MAGGAIN_12GAUSS);

  // 3.) Setup the gyroscope
  lsm.setupGyro(lsm.LSM9DS0_GYROSCALE_245DPS);
  //lsm.setupGyro(lsm.LSM9DS0_GYROSCALE_500DPS);
  //lsm.setupGyro(lsm.LSM9DS0_GYROSCALE_2000DPS);
  return true;
}

void IMU_ClientLSM9DS0::readOneData(IsaTwoRecord& record) {
    DPRINTSLN(DBG_LSM_READ_DATA, "IMU_ClientLSM9DS0::readOneData");
	sensors_event_t accel, mag, gyro, temp;

	lsm.getEvent(&accel, &mag, &gyro, &temp);
	   DPRINTSLN(DBG_LSM_READ_DATA, "readOneData: after getEvent");

	// Warning: magnetometers use a left-handed referential
	// so we change the sign of the z component.
	// Values provided in the event are in Gauss. Record expects it in µT.
	// 1T = 10000 G <=> 1 µT  =0,01 G <=> 1 G = 100µT
	record.mag[0] = (mag.magnetic.x * 100.0f);
	record.mag[1] = (mag.magnetic.y * 100.0f);
	record.mag[2] = (-mag.magnetic.z * 100.0f);

	record.accelRaw[0] = lsm.accelData.x;
	record.accelRaw[1] = lsm.accelData.y;
	record.accelRaw[2] = lsm.accelData.z;

	record.gyroRaw[0] = lsm.gyroData.x;
	record.gyroRaw[1] = lsm.gyroData.y;
	record.gyroRaw[2] = lsm.gyroData.z;

	   DPRINTSLN(DBG_LSM_READ_DATA, "out of readOneData");

}
