/*
 * IMU_FusionFilter.h
 */

#pragma once
#include "IsaTwoConfig.h"

#if AHRS_FUSION_ALGORITHM==AHRS_FUSION_MADGWICK
#  include "MadgwickAHRS.h"
#elif AHRS_FUSION_ALGORITHM==AHRS_FUSION_MAHONY
#  include "MahonyAHRS.h"
#else
#  error "Unexpected fusion algorithm in IMU_FusionFilter.h"
#endif

#include "Adafruit_Sensor.h"
#ifdef USE_NXP_PRECISION_IMU
#  include "Adafruit_FXOS8700.h"
#  include "Adafruit_FXAS21002C.h"
#else
#  include "Adafruit_LSM9DS0.h"
#endif
#include "elapsedMillis.h"

/** @brief IMU data acquisition, calibration and fusion using the Madgwick Quaternion update algorithm.
 *  A class combining the acquisition of IMU information from LSM9DS0 or PrecisionNXP board (depending
 *  on the USE_NXP_PRECISION symbol in IsaTwoConfig.h), calibration using data from
 *  IMU_LSM_Calibration.h or IMU_NXP_Calibration.h, and fusion to obtain Euler angles.
 */
class IMU_FusionFilter {
public:
	IMU_FusionFilter();
	/** Initialise the object.
	 *  @param updatePeriodInMsec The delay between two successive calls to update (update should happen
	 *         regularly and frequently (maximum delay: TO BE COMPLETED)
	 */
	bool begin(unsigned int updatePeriodInMsec);
	/** Preform one reading, calibration and Euler angles update */
	void update() ;

	/** Get current value of Roll angle
	 * @return Roll angle in radians.
	 */
	inline float getRoll() { return filter.getRollRadians(); };

	/** Get current value of Yaw angle
	 * @return Yaw angle in radians.
	 */
	inline float getYaw() { return filter.getYawRadians(); };

	/** Get current value of Pitch angle
	 * @return Pitch angle in radians.
	 */
	inline float getPitch() { return filter.getPitchRadians(); };
	float calibratedMag[3],calibratedAccel[3],calibratedGyro[3];
protected:
	/** Calibrate magnetometer readings using offsets and transformation matrix */
	void calibrateMag(const float& magX, const float& magY, const float& magZ, float calibrated[3]) ;

	/** Calibrate gyroscope readings using Zero-rates and resolution */
	void calibrateGyro(const float& gyroX, const float& gyroY, const float& gyroZ, float calibrated[3]) ;

	/** Calibrate accelerometer readings using Zero-G and resolution */
	void calibrateAccel(const float& accelX, const float& accelY, const float& accelZ, float calibrated[3]) ;
	
	
private:
#if AHRS_FUSION_ALGORITHM==AHRS_FUSION_MADGWICK
	Madgwick filter;		/**< The internal instance of the Madgwick filter. */
#else
	Mahony filter;		/**< The internal instance of the Mahony filter. */
#endif
	unsigned int updatePeriodInMsec; /**< The theoretical update period for the filter */
	elapsedMillis elapsedSinceUpdate; /**< Actual time elapsed since the last update */
#ifdef USE_NXP_PRECISION_IMU
    Adafruit_FXOS8700 accelmag;
    Adafruit_FXAS21002C gyro;
#else
	Adafruit_LSM9DS0 lsm;   // The LSM9DS0 driver
#endif
};
