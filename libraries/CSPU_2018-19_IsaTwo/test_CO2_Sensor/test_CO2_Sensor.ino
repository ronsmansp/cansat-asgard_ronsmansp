byte sensorPin = A1;
float previousVoltage = 0.0;
float ts = 0;

void PrintNormalResults(int outputDelay) {
  int rawSensorValue = analogRead(sensorPin);
  float voltage = rawSensorValue * (5000 / 1023.0);

  if (voltage == 0) {
    Serial.println ("Error. Please Check Sensor");
  } else if (voltage < 400) {
    Serial.println ("Pre-heating sensor, Please Wait . . .");
  } else {
    float voltage_difference = voltage - 400;
    float concentration = (voltage_difference * 50.0) / 16.0;
    float delta = voltage - previousVoltage;
    float deltaConcentration = (delta * 50.0) / 16.0;
    Serial.print("voltage:");
    Serial.print(voltage);
    Serial.print(" mv, delta=");
    Serial.print(delta);
    Serial.print(", concentration: ");
    Serial.print(concentration);
    Serial.print(" ppm, delta=");
    Serial.println(deltaConcentration);
    previousVoltage = voltage;
  }
  delay(outputDelay);
}

void csvResults(int outputDelay) {
  int rawSensorValue1 = analogRead(sensorPin);
  float voltage1 = rawSensorValue1 * (5000 / 1023.0);
  float voltage_difference1 = voltage1 - 400;
  float concentration1 = (voltage_difference1 * 50.0) / 16.0; //SEE WHY 50 AND 16 TO SEE IF IT'S BOARD DEPENDING
  Serial.print (ts);
  Serial.print (", ");
  Serial.println (concentration1);
  delay (outputDelay);
  ts += 1000;
}

void setup() {
  Serial.begin(115200);
}

void loop() {
  csvResults(1000);
}
