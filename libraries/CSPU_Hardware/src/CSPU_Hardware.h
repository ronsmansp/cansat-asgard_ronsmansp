/*
 * CSPU_Hardware.h
 * 
 * This files is only used for documentation of the library using Doxygen.
 * Every class in the library should include a tag @ingroup CSPU_HW_Scanner
 * in the class documentation block.
 */

 /** @defgroup CSPU_Hardware CSPU_Hardware library
 *  @brief A class to explore the hardware connected to a board and interact with it.
 *  
 *  
 *  _Dependencies_\n
 *  This library requires the following other generic libraries (in addition to hardware-specific libraries,
 *  and standard Arduino libraries, which are not listed here):
 *  - CSPU_Debug
 *  - elapsedMillis
 *  
 *  
 *  _History_\n
 *  The library was created by the 2017-18 Cansat team (ISATIS) and further enriched
 *  during the next projects.
 *  The library was split in several dedicated libraries in Sept. 2021.
 */

