#pragma once
#include "CansatRecord.h"
#include "TorusInterface.h"

/** @ingroup TorusCSPU
    @brief The record carrying all data acquired or computed by the CanSat
*/
class TorusRecord : public CansatRecord {
  public:
    uint16_t parachuteRopeLen; /**< By how much is the rope holding the parachute actually extended (mm) */
    uint16_t parachuteTargetRopeLen; /**< The target parachute extension (mm) */
    TorusControlCode	controllerInfo; /**< Information about the flight controller status */
    uint8_t	flightPhase;  /**< A number identifying the flight phase, valid only if
     	 	 	 	 	 	   the controllerInfo is TorusControlCode::NominalDescent.
     	 	 	 	 	 	   Valid values are 0-15 (coded on 4-bits in binary version)
    						   15 is reserved as "No data value". */
  protected:
    virtual void printCSV_SecondaryMissionData(Stream &str, bool startWithSeparator, bool finalSeparator) const;
    virtual void printCSV_SecondaryMissionHeader(Stream &str, bool startWithSeparator, bool finalSeparator) const;
    virtual void clearSecondaryMissionData();
    virtual uint16_t getSecondaryMissionMaxCSV_Size() const;
    virtual uint16_t getSecondaryMissionCSV_HeaderSize() const;
    virtual void printSecondaryMissionData(Stream& str) const;
    virtual uint8_t writeBinarySecondaryMissionData(uint8_t* const destinationBuffer, uint8_t bufferSize) const;
    virtual uint8_t readBinarySecondaryMissionData(const uint8_t* const sourceBuffer, const uint8_t bufferSize);
    virtual uint8_t getBinarySizeSecondaryMissionData() const;

    friend class CansatRecord_Test;
};
