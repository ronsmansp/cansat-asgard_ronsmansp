/*
 * CSPU_Debug.h
 * This header file only allows the use of CSPU_Debug.h as an alias for DebugCSPU.h
 * (which is more consistent with the name of the library).
 *
 * This files is also used for documentation of the library using Doxygen.
 * Every class in the library should include a tag @ingroup CSPU_Debug
 * in the class documentation block.
 */

#pragma once
#include "DebugCSPU.h"

 /** @defgroup CSPU_Debug CSPU_Debug library
 *  @brief The library of general purpose classes, relevant to any Cansat or Asgard project.
 *
 *  The Cansat-Asgard library contains various high-level classes without any specific link to a particular project:
 *  - Interface to sensors (without enforcing project-specific configuration)
 *  - Generic classes to manage the hardware
 *  - Generic classes to implement an AHRS system
 *  - Generic classes to support the on-board processing cycle
 *  - ....
 *
 *  _Dependencies_\n
 *  This library requires the following other generic libraries (in addition to hardware-specific libraries,
 *  and standard Arduino libraries, which are not listed here):
 *  - None
 *
 *
 *  _History_\n
 *  The library was created by the 2017-18 Cansat team (ISATIS) and further enriched
 *  during the next projects.
 */

