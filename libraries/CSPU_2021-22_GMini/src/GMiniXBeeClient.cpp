/*
 * GMiniXBeeClient.cpp
 */
#include "CansatConfig.h"  // Include here to have the definition of RF_ACTIVATE_API_MODE
#ifdef RF_ACTIVATE_API_MODE
#include "GMiniXBeeClient.h"

bool GMiniXBeeClient::doCheckXBeeModuleConfiguration(
			bool correctConfiguration,
			bool &configurationChanged,
			const uint32_t &sh, const uint32_t &sl) {
	bool result=checkCommonModuleConfiguration(correctConfiguration, configurationChanged, sh, sl);
	auto component=getXBeeSystemComponent(sh,sl);
	switch(component) {
	case GMiniComponent::MainCan:
		if (!checkCanModuleConfiguration(correctConfiguration, configurationChanged,sh,sl)) {
			result=false;
		}
		break;
	case GMiniComponent::SubCan1:
		if (!checkSubCanModuleConfiguration(CansatCanID::SubCan1, correctConfiguration, configurationChanged,sh,sl)){
			result=false;
		}
		break;
	case GMiniComponent::SubCan2:
		if (!checkSubCanModuleConfiguration(CansatCanID::SubCan2, correctConfiguration, configurationChanged,sh,sl)){
			result=false;
		}
		break;
	case GMiniComponent::SubCan3:
		if (!checkSubCanModuleConfiguration(CansatCanID::SubCan3, correctConfiguration, configurationChanged,sh,sl)){
			result=false;
		}
		break;
	case GMiniComponent::SubCan4:
		if (!checkSubCanModuleConfiguration(CansatCanID::SubCan4, correctConfiguration, configurationChanged,sh,sl)){
			result=false;
		}
		break;

	case GMiniComponent::RF_Transceiver:
		if (!checkGroundModuleConfiguration(correctConfiguration, configurationChanged,sh,sl)) {
			result=false;
		}
		break;
	case GMiniComponent::Undefined:
		result=false;
		Serial << "*** Unidentified system component: " << getLabel(component) << ENDL;
		break;
	default:
		result=false;
		Serial << "*** Unexpected system component (" << (uint8_t) component << ")" << ENDL;
	}
	return result;
}

GMiniXBeeClient::GMiniComponent GMiniXBeeClient::getXBeeSystemComponent(
		const uint32_t &sh, const uint32_t &sl) {
	uint32_t mySH=sh, mySL=sl;
	if ((mySH==0)&&(mySL==0)) {
		if ((!queryParameter("SH", mySH)) || (!queryParameter("SL", mySL)) ) {
			DPRINTSLN(DBG_DIAGNOSTIC, "Cannot retrieve XBee address");
			return GMiniComponent::Undefined;
		}
	}

	if ((mySH == GM_MainCan_XBeeAddressSH) && (mySL == GM_MainCan_XBeeAddressSL)) {
		return GMiniComponent::MainCan;
	}
	if ((mySH == GM_SubCan1_XBeeAddressSH) && (mySL == GM_SubCan1_XBeeAddressSL)) {
		return GMiniComponent::SubCan1;
	}
	if ((mySH == GM_SubCan2_XBeeAddressSH) && (mySL == GM_SubCan2_XBeeAddressSL)) {
		return GMiniComponent::SubCan2;
	}
	if ((mySH == GM_SubCan3_XBeeAddressSH) && (mySL == GM_SubCan3_XBeeAddressSL)) {
		return GMiniComponent::SubCan3;
	}
	if ((mySH == GM_SubCan4_XBeeAddressSH) && (mySL == GM_SubCan4_XBeeAddressSL)) {
		return GMiniComponent::SubCan4;
	}
	if ((mySH == GM_Ground_XBeeAddressSH) && (mySL == GM_Ground_XBeeAddressSL)) {
		return GMiniComponent::RF_Transceiver;
	}
	if ((mySH == GM_Ground2_XBeeAddressSH) && (mySL == GM_Ground2_XBeeAddressSL)) {
		return GMiniComponent::RF_Transceiver;
	}
	return GMiniComponent::Undefined;
}

bool GMiniXBeeClient::printConfigurationSummary(Stream &stream,
		const uint32_t & sh, const uint32_t& sl) {

	if (!CansatXBeeClient::printConfigurationSummary(stream,sh,sl)) {
		return false;
	}
	stream << "  According to GMiniConfig.h: " << ENDL;
	stream << "    Role of this module: " << getLabel(getXBeeSystemComponent(sh,sl)) << ENDL;
	stream << "    Applied RF strategy: " << getLabel(GMiniSelectedRF_Strategy) << ENDL;
	return  true;
}

bool GMiniXBeeClient::checkCommonModuleConfiguration(
		bool correctConfiguration, bool &configurationChanged,
		const uint32_t& sh,const uint32_t& sl) {
	bool result=CansatXBeeClient::checkCommonModuleConfiguration(
			correctConfiguration,configurationChanged,sh,sl);

	// Add here any GMini-specific configuration common to all modules.
	return result;
}

bool GMiniXBeeClient::checkCanModuleConfiguration(
		bool correctConfiguration, bool &configurationChanged,
		const uint32_t& sh,const uint32_t& sl) {
	bool success=true;

	char name[10]="nn_CAN_x";
	updateModuleID_Prefix(name,sh,sl);
	name[7]=RF_XBEE_MODULES_SET;
	if (!checkParameter(	"NI",
			name,
			correctConfiguration,
			configurationChanged,
			sh,sl)) success=false;
	// Do not change name, in order to keep the general Cansat name.
	switch(GMiniSelectedRF_Strategy) {
	case GMiniRF_Strategy::MainCanAsRepeater:
	case GMiniRF_Strategy::DoubleTransmissionFromSubcans:
		// Do not check DH-DL: it's not relevant in API mode.
		// SubCan must be the Coordinator.
		if (!checkDevicetype(	XBeeClient::DeviceType::Coordinator,
								correctConfiguration,
								configurationChanged,
								sh,sl)) success = false;
		break;
	case GMiniRF_Strategy::BroadcastFromSubcans:
	case GMiniRF_Strategy::MulticastFromSubcans:
	case GMiniRF_Strategy::MultipleBindingsFromSubcans:
		Serial <<"*** Current RF strategy (" << getLabel(GMiniSelectedRF_Strategy)
			   << ") is not supported for Main Can" << ENDL;
		break;
	default:
		Serial << "Unexpected RF_Strategy (" << (uint8_t)GMiniSelectedRF_Strategy  << ")" << ENDL;
	}
	return success;
}

bool GMiniXBeeClient::checkSubCanModuleConfiguration(
		CansatCanID subCanID,
		bool correctConfiguration, bool &configurationChanged,
		const uint32_t& sh,const uint32_t& sl) {
	bool success=true;

	char name[12]="nn_SUB_x_X";
	updateModuleID_Prefix(name,sh,sl);
	name[7]='0'+ (int) subCanID;
	name[9]=RF_XBEE_MODULES_SET;
	if (!checkParameter(	"NI",
							name,
							correctConfiguration,
							configurationChanged,
							sh,sl)) success=false;
	switch(GMiniSelectedRF_Strategy) {
	case GMiniRF_Strategy::MainCanAsRepeater:
	case GMiniRF_Strategy::DoubleTransmissionFromSubcans:
		// Do not check DH-DL: it's not relevant in API mode.
		// SubCan must be a router.
		if (!checkDevicetype(	XBeeClient::DeviceType:: Router,
								correctConfiguration,
								configurationChanged,
								sh,sl)) success = false;
		break;
	case GMiniRF_Strategy::BroadcastFromSubcans:
	case GMiniRF_Strategy::MulticastFromSubcans:
	case GMiniRF_Strategy::MultipleBindingsFromSubcans:
		Serial <<"*** Current RF strategy (" << getLabel(GMiniSelectedRF_Strategy)
			   << ") is not supported for SubCan" << ENDL;
		break;
	default:
		Serial << "Unexpected RF_Strategy (" << (uint8_t)GMiniSelectedRF_Strategy  << ")" << ENDL;
	}
	return success;
}

bool GMiniXBeeClient::checkGroundModuleConfiguration(
		bool correctConfiguration, bool &configurationChanged,
		const uint32_t& sh,const uint32_t& sl) {
	bool success=true;

	char name[14]="nn_RF-TSCV_x";
	updateModuleID_Prefix(name,sh,sl);
	name[11]=RF_XBEE_MODULES_SET;
	if (!checkParameter(	"NI",
							name,
							correctConfiguration,
							configurationChanged,
							sh,sl)) success=false;

	// Do not change name, in order to keep the general Cansat name.
	switch(GMiniSelectedRF_Strategy) {
	case GMiniRF_Strategy::MainCanAsRepeater:
	case GMiniRF_Strategy::DoubleTransmissionFromSubcans:
		// Do not check DH-DL: it's not relevant in API mode.
		// Ground must be a router.
		if (!checkDevicetype(	XBeeClient::DeviceType::Router,
								correctConfiguration,
								configurationChanged,
								sh,sl)) success = false;
		break;
	case GMiniRF_Strategy::BroadcastFromSubcans:
	case GMiniRF_Strategy::MulticastFromSubcans:
	case GMiniRF_Strategy::MultipleBindingsFromSubcans:
		Serial <<"*** Current RF strategy (" << getLabel(GMiniSelectedRF_Strategy)
			   << ") is not supported for RF-Transceiver" << ENDL;
		break;
	default:
		Serial << "Unexpected RF_Strategy (" << (uint8_t)GMiniSelectedRF_Strategy  << ")" << ENDL;
	}

	return success;
}

const char* GMiniXBeeClient::getLabel(GMiniComponent component) {
	switch (component) {
	case GMiniComponent::MainCan:
		return "MAIN CAN";
		break;
	case GMiniComponent::SubCan1:
		return "SUBCAN 1";
		break;
	case GMiniComponent::SubCan2:
		return "SUBCAN 2";
		break;
	case GMiniComponent::SubCan3:
		return "SUBCAN 3";
		break;
	case GMiniComponent::SubCan4:
		return "SUBCAN 4";
		break;
	case GMiniComponent::RF_Transceiver:
		return "GROUND STATION";
		break;
	case GMiniComponent::Undefined:
		return "UNIDENTIFIED";
		break;
	default:
		return "Unknown";
		DPRINTS(DBG_DIAGNOSTIC, "Unexpected system component: ");
		DPRINTLN(DBG_DIAGNOSTIC,(uint8_t) component);
	}
}

const char* GMiniXBeeClient::getLabel(GMiniRF_Strategy strategy) {
	switch(strategy) {
	case GMiniRF_Strategy::MainCanAsRepeater:
		return "Main can as repeater";
		break;
	case GMiniRF_Strategy::BroadcastFromSubcans:
		return "Broadcast from subcans";
		break;
	case GMiniRF_Strategy::MulticastFromSubcans:
		return "Multicast from subcans";
		break;
	case GMiniRF_Strategy::MultipleBindingsFromSubcans:
		return "Multiple bindings from subcans";
		break;
	case GMiniRF_Strategy::DoubleTransmissionFromSubcans:
		return "Double transmission from subcans";
		break;
	default:
		return "UNKNOWN";
		DPRINTS(DBG_DIAGNOSTIC, "Unexpected RF_Strategy: ");
		DPRINTLN(DBG_DIAGNOSTIC,(uint8_t) GMiniSelectedRF_Strategy);
	}
}
#endif
