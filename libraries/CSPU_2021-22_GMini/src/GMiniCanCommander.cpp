/*
   GMiniCanCommander.cpp
*/
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#define DEBUG_CSPU
#include "DebugCSPU.h"
#include "CansatInterface.h"
#include "GMiniCanCommander.h"
#include "CansatXBeeClient.h"

#ifdef GMINI_SUPPORT_LATCH_COMMANDS
#include "GMiniMainAcquisitionProcess.h"
#endif

#define DBG_READ_MSG 1
#define DBG_DIAGNOSTIC 1

GMiniCanCommander::GMiniCanCommander(unsigned long int theTimeOut) :
  RT_CanCommander(theTimeOut) {
}

#ifdef RF_ACTIVATE_API_MODE
void GMiniCanCommander::begin(CansatXBeeClient &xbeeClient, SdFat* theSd,
                              CansatAcquisitionProcess* theProcess) {
  RT_CanCommander::begin(xbeeClient, theSd, theProcess);
#ifdef GMINI_SUPPORT_LATCH_COMMANDS
  GMiniMainAcquisitionProcess* gminiProcess = (GMiniMainAcquisitionProcess*) theProcess;
  servo = gminiProcess->getServoLatch();
#endif
}
#else
void GMiniCanCommander::begin (Stream& theResponseStream, SdFat* theSd, CansatAcquisitionProcess* theProcess) {
  RT_CanCommander::begin(theResponseStream, theSd, theProcess);
#ifdef SUPPORT_LATCH_COMMANDS
  GMiniMainAcquisitionProcess* gminiProcess = (GMiniMainAcquisitionProcess*) theProcess;
  servo = gminiProcess->getServoLatch();
#endif
}
#endif

#ifdef GMINI_SUPPORT_LATCH_COMMANDS
void GMiniCanCommander::processReq_GetServoLatchStatus(char* &nextCharAddress) {

  DPRINTSLN(DBG_READ_MSG, "GetServoLatchStatus");
  ServoLatch::LatchStatus status = servo->getStatus();
  RF_OPEN_CMD_RESPONSE(RF_Stream);
  *RF_Stream << (int)CansatCmdResponseType::ServoLatch_LatchStatus << "," <<  (int)status
             << "," ;
  switch (status) {
    case ServoLatch::LatchStatus::Neutral:
      *RF_Stream << "Neutral";
      break;
    case ServoLatch::LatchStatus::Locked:
      *RF_Stream << "Locked";
      break;
    case ServoLatch::LatchStatus::Unlocked:
      *RF_Stream << "Unlocked";
      break;
    case ServoLatch::LatchStatus::Undefined:
      *RF_Stream << "Undefined";
      break;
    default:
      *RF_Stream << "Unexpected value";
      break;
  }
  RF_CLOSE_CMD_RESPONSE(RF_Stream);
}

void GMiniCanCommander::processReq_ServoLatch_Lock(char* &nextCharAdress) {
  DPRINTSLN(DBG_READ_MSG, "ServoLatch_Lock");
  servo->lock();
  RF_OPEN_CMD_RESPONSE(RF_Stream);
  *RF_Stream << (int)CansatCmdResponseType::ServoLatch_LatchStatus << "," << (int)ServoLatch::LatchStatus::Locked << ",Locked" ;
  RF_CLOSE_CMD_RESPONSE(RF_Stream);
}

void GMiniCanCommander::processReq_ServoLatch_Unlock(char* &nextCharAdress) {
  DPRINTSLN(DBG_READ_MSG, "ServoLatch_Unlock");
  servo->unlock();
  RF_OPEN_CMD_RESPONSE(RF_Stream);
  *RF_Stream << (int)CansatCmdResponseType::ServoLatch_LatchStatus << "," << (int)ServoLatch::LatchStatus::Unlocked << ",Unlocked" ;
  RF_CLOSE_CMD_RESPONSE(RF_Stream);
}

void GMiniCanCommander::processReq_ServoLatch_SetNeutral(char* &nextCharAdress) {
  DPRINTSLN(DBG_READ_MSG, "ServoLatch_SetNeutral");
  servo->neutral();
  RF_OPEN_CMD_RESPONSE(RF_Stream);
  *RF_Stream << (int)CansatCmdResponseType::ServoLatch_LatchStatus << "," << (int)ServoLatch::LatchStatus::Neutral << ",Neutral";
  RF_CLOSE_CMD_RESPONSE(RF_Stream);
}
#endif

bool GMiniCanCommander::processProjectCommand(CansatCmdRequestType requestType,
    char* cmd) { 
      DPRINTS(DBG_READ_MSG, "process GMiniProjectCommand ");
  bool result = true; 

  switch (requestType) {
#ifdef GMINI_SUPPORT_LATCH_COMMANDS
    case CansatCmdRequestType::GetServoLatchStatus:
      processReq_GetServoLatchStatus(cmd);
      break;

    case CansatCmdRequestType::ServoLatch_Lock:
      processReq_ServoLatch_Lock(cmd);
      break;

    case CansatCmdRequestType::ServoLatch_Unlock:
      processReq_ServoLatch_Unlock(cmd);
      break;

    case CansatCmdRequestType::ServoLatch_SetNeutral:
      processReq_ServoLatch_SetNeutral(cmd);
      break;
#endif

    default:
      // Do not report error here: this is done by the superclass.
      result = false;
  } // Switch
  return result;
}
