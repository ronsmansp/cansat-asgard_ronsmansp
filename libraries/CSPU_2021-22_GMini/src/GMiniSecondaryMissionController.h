/*
   GMiniSecondaryMissionController.h
*/

#pragma once

#include "SecondaryMissionController.h"
#include "GMiniRecord.h"
#include "ServoLatch.h"
#include "GMiniConfig.h"

/** @brief our secondary mission manager for the Gmini Project */
class GMiniSecondaryMissionController : public SecondaryMissionController {
  public:
    virtual bool begin() override {
      bool result = SecondaryMissionController::begin();
      if (!result) return false;
      subCanEjected = false;
      result = servo.begin(ServoLatchPWM_Pin, ServoLatchminPulseWidth, ServoLatchmaxPulseWidth);
      if (result == false) return false;
      servo.configure(ServoLatchamplitude, ServoLatchneutralPosition);
      return true;
    };
    
    ServoLatch* getServoLatch() {
      return &servo;
    };

  protected:
    virtual void manageSecondaryMission(CansatRecord& record) override {
      Serial << millis() << ": ~GMiniMissionController::manageSecondaryMission() called with GMiniRecord" << ENDL;
      // cast the record to xxxRecord and work with it: something like doSomething((xxxRecord&) record);
      // NB: Proper dynamic casting is not available with Arduino (RTTI is disabled to save memory,
      // so you cannot write xxxRecord& myXxxRecord = dynamic_cast<xxxRecord&>(record);
      GMiniRecord& myGMiniRecord = (GMiniRecord&) record;
      // Use myXxxRecord here
      myGMiniRecord.subCanEjected = false;
    } ;


  private:
    bool subCanEjected;
    ServoLatch servo;
};
