/**
   XBeeClient.cpp
   Started on 21 April 2019
*/
// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "XBeeClient.h" // This one must be outside the ifdef to have the symbol defined or undefined.

#ifdef RF_ACTIVATE_API_MODE

#include <XBee.h>
#include "elapsedMillis.h"

#define DBG_SEND 		0
#define DBG_NO_TIMELY_TX_STATUS 0 // Activate to have a message when ack time-out expires
#define DBG_RECEIVE 	0
#define DBG_STREAM 		0
#define DBG_DIAGNOSTIC 	1
#define DBG_QUERY 		0
#define DBG_REMOTE_QUERY 0
#define DBG_SET 		0
#define DBG_RESTORE		0
#define DBG_RETRIES		0
#define DBG_CHECK		0

static constexpr uint32_t AT_ResponseDelay=2000; // ms.
static constexpr uint32_t RemoteAT_ResponseDelay=2000; // ms.
static constexpr uint8_t MaxStringValueLength=20; // maximum number of characters
												  // in a string parameter value
												  // (including final '\0')

XBeeClient::XBeeClient(uint32_t defaultDestinationSH, uint32_t defaultDestinationSL) :
  xbee(),
  defaultReceiverAddress(defaultDestinationSH, defaultDestinationSL),
  msgBuffer(),
  sstr(msgBuffer),
#ifdef ARDUINO_AVR_UNO
  swSerialStream(nullptr),
#endif
  hwSerialStream(nullptr)
{
	msgBuffer.reserve(200);
}

void XBeeClient::begin(HardwareSerial &stream) {
  xbee.begin(stream);
  hwSerialStream=&stream;
}

#ifdef ARDUINO_AVR_UNO
void XBeeClient::begin(SoftwareSerial &stream) {
  xbee.begin(stream);
  swSerialStream=&stream;
}
#endif

bool XBeeClient::send(	uint8_t* data,
                        uint8_t dataSize,
                        int timeOutCheckResponse,
                        uint32_t destSH, uint32_t destSL) {
  if (dataSize > MaxPayloadSize) {
    DPRINTS(DBG_DIAGNOSTIC, "XBeeClient::send: payload too large: ");
    DPRINT(DBG_DIAGNOSTIC, dataSize);
    DPRINTSLN(DBG_DIAGNOSTIC, " bytes");
    return false;
  }

  XBeeAddress64 receiverAddress(destSH, destSL);
  if ((destSH == 0L) && (destSL == 0L)) {
    receiverAddress = defaultReceiverAddress;
  }
#if DBG_SEND == 1
  Serial << "Sending to ";
  PrintHexWithLeadingZeroes(Serial, receiverAddress.getMsb());
  Serial << "-";
  PrintHexWithLeadingZeroes(Serial, receiverAddress.getLsb());
  Serial << ", timeout=" << timeOutCheckResponse << " msec" << ENDL;
#endif

  if (DisplayOutgoingFramesOnSerial) {
	  Serial << "Sending..." << ENDL;
	  printFrame(data,dataSize);
  }

  // Should we give values back ? Like the SD_Logger for instance to know exactly the type of ***ERROR*** it is
  bool result = false;
  ZBTxRequest zbTx = ZBTxRequest(receiverAddress, data, dataSize); //Creating an object to send the data
  ZBTxStatusResponse txStatus = ZBTxStatusResponse();
  xbee.send(zbTx);
  if (timeOutCheckResponse == 0) {
    // If timeout is null we do not check for reception.
    return true;
  }

  if (xbee.readPacket(timeOutCheckResponse)) { //Wait up to timeOutCheckResponse ms a response from the other Xbee
    if (xbee.getResponse().getApiId() == ZB_TX_STATUS_RESPONSE) {
      DPRINTS(DBG_SEND, "We got the ZB_TX_STATUS_RESPONSE...");
      xbee.getResponse().getZBTxStatusResponse(txStatus);
      // get the delivery status, the fifth byte
      uint8_t status = txStatus.getDeliveryStatus();
      DPRINTS(DBG_SEND, "Delivery status = ");
      DPRINTLN(DBG_SEND, status);

      if (txStatus.getDeliveryStatus() == SUCCESS) {
#if (DBG_SEND==1)
        Serial << F("SUCCESS, delivered to 16-bit address ");
        PrintHexWithLeadingZeroes(Serial, txStatus.getRemoteAddress());
        Serial << ENDL;
#endif
        result = true;
      } else {
        // ***ERROR***
        DPRINTSLN(DBG_DIAGNOSTIC, "***ERROR*** The other XBee didn't receive our packet");
      }
    }
  } else if (xbee.getResponse().isError()) {
    // ***ERROR*** we had no response
    DPRINTSLN(DBG_DIAGNOSTIC, "***ERROR*** reading packet. ErrCode=");
    DPRINTLN(DBG_SEND, xbee.getResponse().getErrorCode());
    result = DBG_DIAGNOSTIC;
  } else {
    // ***ERROR***
    DPRINTSLN(DBG_NO_TIMELY_TX_STATUS, "***ERROR*** - No timely TX Status Response");
  }
  return result;
}

bool XBeeClient::receive(uint8_t* &payload, uint8_t& payloadSize) {
  bool result = false;
  static ZBRxResponse rx; 	// Make it static to preserve the object and the buffer which we return as payload.
  	  	  	  	  	  	  	// Could be unnecessary if the internal buffer is a global, but who knows...
  ModemStatusResponse msr;
  xbee.readPacket(); //Checks the communication at every loop
  if (xbee.getResponse().isAvailable()) {
    DPRINTS(DBG_RECEIVE, "We got something: ");
    uint8_t apiId = xbee.getResponse().getApiId();
    if (apiId == ZB_RX_RESPONSE) {
      DPRINTSLN(DBG_RECEIVE, "a zb rx packet!");
      //Now fill our zb rx class
      xbee.getResponse().getZBRxResponse(rx);
      DPRINTS(DBG_RECEIVE, "Remote address: 0x");
      DPRINT(DBG_RECEIVE,  rx.getRemoteAddress64().getMsb());
      DPRINTLN(DBG_RECEIVE,  rx.getRemoteAddress64().getLsb());

      uint8_t resp = rx.getOption();
      if (resp == ZB_PACKET_ACKNOWLEDGED) {
        // the sender got an ACK
        DPRINTSLN(DBG_RECEIVE, "Sender got en ACK - good");

        payload = rx.getData();
        payloadSize = rx.getDataLength();
        if (DisplayIncomingFramesOnSerial) {
        	  Serial << "Received payload:" << ENDL;
        	  printFrame(payload,payloadSize);
          }
        result = true;
      } else {
        //***ERROR***
        DPRINTSLN(DBG_DIAGNOSTIC, "***ERROR*** Received packet but sender didn't get an ACK. API ID=");
        DPRINTLN(DBG_DIAGNOSTIC, apiId);
        DPRINTS(DBG_DIAGNOSTIC, ", ZBRxResponse: ");
        DPRINTLN(DBG_DIAGNOSTIC, resp);
      }
    } else if (apiId == MODEM_STATUS_RESPONSE) {
      xbee.getResponse().getModemStatusResponse(msr);
      // the local XBee sends this response on certain events, like association/dissociation
      DPRINTS(DBG_RECEIVE, "A certain event happened association/dissociation: ");
      auto msrStatus = msr.getStatus() ;
      if (msrStatus == ASSOCIATED) {
        DPRINTSLN(DBG_RECEIVE, "It's an association - Yeay this is great!");
        result = true;
      } else if (msrStatus == DISASSOCIATED) {
        //***ERROR***
        DPRINTSLN(DBG_DIAGNOSTIC, "*** Error MSR status: dissociation");
      } else {
        //***ERROR***
        DPRINTS(DBG_DIAGNOSTIC, "*** Error MSR status unknown: ");
        DPRINTLN(DBG_DIAGNOSTIC, msrStatus);
      }
    }
    else if (apiId == ZB_TX_STATUS_RESPONSE) {
      // This we receive after a message is sent for which we did not wait for
      // the TX_STATUS_RESPONSE because the user did not require an ack.
      // At this stage the message is just ignored.
      DPRINTSLN(DBG_RECEIVE, "(ignored) ZB_TX_STATUS_RESPONSE ");
    }
    else if (apiId == ZB_EXPLICIT_RX_RESPONSE) {
      // This message is only received if JN=1
      DPRINTSLN(DBG_RECEIVE, "ZB_EXPLICIT_RX_RESPONSE");
      DPRINTSLN(DBG_DIAGNOSTIC, "Zigbee network joined!");
    }
    else if (apiId == RouteRecordIndicator) {
        DPRINTSLN(DBG_DIAGNOSTIC, "Received Route Record Indicator (ignored)");
      }
    else {
      DPRINTS(DBG_RECEIVE, "(ignored) Unexpected API ID=0x");
      DPRINTLN(DBG_RECEIVE, apiId, HEX);
    }
  } else if (xbee.getResponse().isError()) {
    //***ERROR***
    DPRINTS(DBG_DIAGNOSTIC, "*** ERROR in reading packet. Err code=");
    DPRINTLN(DBG_DIAGNOSTIC, xbee.getResponse().getErrorCode());
    switch (xbee.getResponse().getErrorCode()) {
      case CHECKSUM_FAILURE:
        DPRINTLN(DBG_DIAGNOSTIC, "Checksum failure");
        break;
      case PACKET_EXCEEDS_BYTE_ARRAY_LENGTH:
        DPRINTS(DBG_DIAGNOSTIC, "Packet exceeds byte-array length. Should not exceed (bytes)  ");
        DPRINTLN(DBG_DIAGNOSTIC, MAX_FRAME_DATA_SIZE - 10);
        break;
      case UNEXPECTED_START_BYTE:
        DPRINTLN(DBG_DIAGNOSTIC, "Unexpected start byte");
        break;
      default:
        DPRINTLN(DBG_DIAGNOSTIC, "Unknown error code.");
    }
  }
  return result;
}

void XBeeClient::openStringMessage(uint8_t type, uint8_t seqNbr) {
  msgBuffer = "  ";		// Reserve 2 bytes.
  msgBuffer[0] = type; // Store value of byte, not string representation.
  msgBuffer[1] = seqNbr; // Optional sequence number transported in this byte
  	  	  	  	  	  	 // which is needed to keep even alignment of the string.
  DPRINTS(DBG_STREAM,  "XBeeClient::openStringMessage. buffer='");
  DPRINT(DBG_STREAM,  (msgBuffer.c_str() + 2));
  DPRINTS(DBG_STREAM,  "', type=");
  DPRINTLN(DBG_STREAM, (uint8_t) msgBuffer[0]);
}

bool XBeeClient::closeStringMessage(int timeOutCheckResponse) {
  DPRINTS(DBG_STREAM,  "XBeeClient::closeStringMessage. buffer='");
  DPRINT(DBG_STREAM,  (msgBuffer.c_str() + 2));
  DPRINTS(DBG_STREAM,  "', type=");
  DPRINTLN(DBG_STREAM, (uint8_t) msgBuffer[0]);

  auto sLength = msgBuffer.length()-2; // First 2 bytes are not the string
  if (sLength == 0) {
    DPRINTS(DBG_DIAGNOSTIC, "Error: closing empty message. Type=");
    DPRINTLN(DBG_DIAGNOSTIC, (uint8_t) msgBuffer[0]);
    return false;
  }
  if (sLength > MaxStringSize) {
    DPRINTS(DBG_DIAGNOSTIC, "Error: string message too long:");
    DPRINT(DBG_DIAGNOSTIC, sLength);
    DPRINTS(DBG_DIAGNOSTIC, ", Max=");
    DPRINT(DBG_DIAGNOSTIC, MaxStringSize);
    DPRINTS(DBG_DIAGNOSTIC, ", ");
    DPRINTLN(DBG_DIAGNOSTIC, (msgBuffer.c_str() + 2));
    return false;
  }

  uint8_t* payload = (uint8_t *) msgBuffer.c_str();
  uint8_t payloadSize = (uint8_t) msgBuffer.length() + 1;
  if (DisplayOutgoingFramesOnSerial) {
 	  printString(payload, payloadSize);
  }

  // Also send the terminating '\0'.
  return send(payload, payloadSize, timeOutCheckResponse);
}

bool XBeeClient::getString(	char* str, uint8_t &stringType,
							uint8_t& stringSeqNbr,
							const uint8_t* data, uint8_t dataSize) const {
	bool result=false;

	if (dataSize <=2) {
		DPRINTS(DBG_DIAGNOSTIC, "*** Error: received empty string");
		*str ='\0';
		return result;
	}
	const char* s = (const char*) (data + 2);
    stringType= data[0];
    stringSeqNbr=data[1];

    if (dataSize != (uint8_t) strlen(s) + 3) {
		// 2 bytes + string + '\0'
		DPRINTS(DBG_DIAGNOSTIC, "*** Error: inconsistent size. size=");
		DPRINT(DBG_DIAGNOSTIC, dataSize);
		DPRINTS(DBG_DIAGNOSTIC, ", String size, from third byte=");
		DPRINTLN(DBG_DIAGNOSTIC, strlen(s));
	} else {
		memcpy(str, data+2, strlen(s)+1);
		result=true;
	}

	return result;
}

bool XBeeClient::isModuleOnline(const uint32_t& sh, const uint32_t& sl) {
	uint8_t value, valueSize=1;
	bool myBool;
    return queryParameterOnce("CE", &value, valueSize, myBool, sh, sl);
}

bool XBeeClient::queryParameter(	const char* param,
									uint8_t value[], uint8_t &valueSize,
									const uint32_t& sh, const uint32_t& sl,
									uint8_t maxRetries) {
	bool result, doNotRetry=true;

	do {
		DPRINTS(DBG_RETRIES,"Attempting query of ");
		DPRINTLN(DBG_RETRIES,param);
		result=queryParameterOnce(param,value,valueSize,doNotRetry,sh,sl);
	} while (!result && !doNotRetry && maxRetries--);
	return result;
}

bool XBeeClient::queryParameter(const char* param, uint64_t &value,const uint32_t& sh, const uint32_t& sl) {
	return queryIntegerParameter(param, value, (uint8_t) 8,sh,sl);
}

bool XBeeClient::queryParameter(const char* param, uint32_t &value,const uint32_t& sh, const uint32_t& sl) {
	DPRINTSLN(DBG_QUERY, "QueryParameter/uint32_t");
	uint64_t longVal=value;
	bool result= queryIntegerParameter(param, longVal, (uint8_t)4,sh,sl);
	value=(uint32_t)longVal;
	return result;
}

bool XBeeClient::queryParameter(const char* param, uint16_t &value,const uint32_t& sh, const uint32_t& sl) {
	uint64_t longVal=value;
	bool result= queryIntegerParameter(param, longVal, (uint8_t)2,sh, sl);
	value=(uint16_t)longVal;
	return result;
}

bool XBeeClient::queryParameter(const char* param, uint8_t &value,const uint32_t& sh, const uint32_t& sl) {
	uint64_t longVal=value;
	bool result= queryIntegerParameter(param,longVal, (uint8_t)1, sh, sl);
	value= (uint8_t) longVal;
	return result;
}

bool XBeeClient::queryParameter(const char* param, bool &value,const uint32_t& sh, const uint32_t& sl) {
	uint8_t hexVal[1];
	uint8_t hexValSize=1;
	bool result=queryParameter(param, hexVal, hexValSize,sh,sl);
	if (result) {
		value= (hexVal[0] != 0);
	}
	return result;
}

bool XBeeClient::queryParameter(const char* param,
								char value[], uint8_t valueSize,
								const uint32_t& sh, const uint32_t& sl){
	uint8_t hexVal[valueSize-1];
	uint8_t hexValSize=valueSize-1;

	bool result=queryParameter(param, hexVal, hexValSize, sh, sl);
	if (result) {
		for (int i = 0; i<hexValSize; i++) {
			value[i]=(char) hexVal[i];
		}
		value[hexValSize]='\0';
	}
	return result;
}

bool XBeeClient::printConfigurationSummary(Stream &stream, const uint32_t & sh, const uint32_t& sl) {
	constexpr uint8_t MaxStringValueLength=20;
	char sValue[MaxStringValueLength];
	uint64_t iValue1, iValue2;
	bool CE_Value, AO_Value, bValue;
	uint8_t SM_Value;
	bool globalResult=true;

	stream << F("-- Summary of XBee configuration: --") << ENDL;
	if (!queryParameter("NI", sValue, MaxStringValueLength, sh, sl)){
		strcpy(sValue, "*** Error ***"); // Do not exceed MaxStringValueLength-1 !
		return false;  // If the first param fails (after retries)
					   // We'll assume there is no XBee on the serial line.
	}
	stream << F("  NI    : '") << sValue << F("' (Human-readable name)") << ENDL;

	if (!queryParameter("SH", iValue1,sh,sl)){
		iValue1=0;
		globalResult = false;
	}
	stream << F("  SH-SL : "); PrintHexWithLeadingZeroes(stream, (uint32_t) iValue1);
	if (!queryParameter("SL", iValue1, sh, sl)){
		iValue1=0;
		globalResult = false;
	}
	stream << " - "; PrintHexWithLeadingZeroes(stream,  (uint32_t) iValue1); stream << ENDL;


	stream << F("  DH-DL : not relevant in API mode!") << ENDL;

	if (!queryParameter("SC", iValue1, sh, sl)){
		iValue1=0;
		globalResult = false;
	}
	stream << F("  SC    : 0x"); stream.print((unsigned long) iValue1, HEX);
	stream << F(" (Scanned channels: ");
	printScannedChannels(stream, iValue1);
	stream << F(")") << ENDL;

	if (!queryParameter("ZS", iValue1, sh, sl)){
		iValue1=0;
		globalResult = false;
	}
	stream << F("  ZS    : 0x"); stream.print((unsigned long) iValue1, HEX);
	stream << F(" (ZigBee Stack profile)") << ENDL;

	if (!queryParameter("D6", iValue1, sh, sl)){
		iValue1=0;
		globalResult = false;
	}
	stream << F("  D6-D7 : 0x"); stream.print((unsigned long) iValue1, HEX);
	if (!queryParameter("D7", iValue1, sh, sl)){
		iValue1=0;
		globalResult = false;
	}
	stream << F(" - 0x"); stream.print((unsigned long) iValue1, HEX);
	stream << F(" (0=Unmonitored IO pins)") << ENDL;


	if (!queryParameter("BD", iValue1, sh, sl)){
		iValue1=0;
		globalResult = false;
	}
	stream << F("  BD    : "); stream.print((unsigned long) iValue1);
	stream << F(" (Baud rate on serial line, 7=115200 baud)") << ENDL;

	if (!queryParameter("AP", iValue1, sh, sl)){
		iValue1=0;
		globalResult = false;
	}
	stream << F("  AP    : 0x"); stream.print((unsigned long) iValue1, HEX);
	stream << F(" (API Enabled 0=disabled, 1=enabled, 2=enabled with escape chars)") << ENDL;

	if (!queryParameter("JV", bValue, sh, sl)){
		globalResult = false;
		stream << F("  JV    : *** Error ***");

	} else stream << F("  JV    : ") << (int) bValue ;
    stream << F(" (Channel verification by Rooter/End device)") << ENDL;

	bool bResult=true;
	if (!queryParameter("CE", CE_Value, sh, sl)){
		globalResult = false;
		bResult = false;
	}
	if (!queryParameter("SM", SM_Value, sh, sl)){
		globalResult = false;
		bResult=false;
	}
	if (bResult) {
		stream << F("  CE-SM : ") << CE_Value << " - " << SM_Value << " ";
		stream << F("  Device type: ") << getLabel(getDeviceType());
	} else {
		stream << F("  CE-SM : *** Error ***");
	}
	stream << ENDL;

	if (!queryParameter("ID", iValue1, sh, sl)) {
		iValue1=0;
		globalResult = false;
	}
	stream << F("  ID-OP : 0x"); stream.print((unsigned long) iValue1, HEX);
	if (!queryParameter("OP", iValue1, sh, sl)) {
		iValue1=0;
		globalResult = false;
	}
	stream << F(" - 0x"); stream.print((unsigned long) iValue1, HEX);
	stream << F(" (PAN ID/Operational PAN ID)") << ENDL;

	if (!queryParameter("AO", AO_Value, sh, sl)) {
		globalResult=false;
		stream << F("  AO    : *** Error *** ") << ENDL;
	} else stream << F("  AO    : ") << (int) AO_Value;
	stream << F(" (If 1, msg is broadcasted when joining network)") << ENDL;

	if (!queryParameter("CH", iValue1, sh, sl)) {
		globalResult=false;
		stream << F("  CH    : *** Error *** ") << ENDL;
	} else stream << F("  CH    : "); stream.print((unsigned long) iValue1);
	stream << F(" (current channel (R-Only), 0=no PAN joined)") << ENDL;

	if (!queryParameter("MY", iValue1, sh, sl)) {
		globalResult=false;
		stream << F("  MY    : *** Error *** ") << ENDL;
	} else stream << F("  MY    : 0x"); stream.print((uint16_t) iValue1, HEX);
	stream << F(" (16-bit network address, 0xFFFE=no PAN joined, 0x0=coord.)") << ENDL;

	if (!queryParameter("NH", iValue1, sh, sl)) {
		globalResult=false;
		stream << F("  NH    : *** Error *** ") << ENDL;
	} else stream << F("  NH    : 0x"); stream.print((uint8_t) iValue1, HEX);
	stream << F(" (unicast hops. Timeout=(50*NH)+100 msec, currently ") << ((50*(uint8_t) iValue1)+100)/1000.0 << F(" s)") << ENDL;

	bResult=true;
	if (!queryParameter("VR", iValue1, sh, sl)) {
		bResult=false;
		globalResult=false;
	}
	if (!queryParameter("HV", iValue2, sh, sl)) {
		bResult=false;
		globalResult=false;
	}
	if (bResult) {
		stream << F("  VR-HV : 0x");
		stream.print((uint16_t) iValue1, HEX);
		stream << " - 0x";
		stream.print((uint16_t) iValue2, HEX);
		stream << F(" (firmware version - Hardware version (R-Only))") << ENDL;
	} else stream << F("  VR-HV : *** Error *** ") << ENDL;

	return globalResult;
}

void XBeeClient::printScannedChannels(Print &stream, uint16_t SC_Value) {
	bool first=true;
	for (int i=0; i< 16; i++) {
		if (SC_Value & 0x1) {
			if (!first) stream << "-";
			first=false;
			stream << 11+i;
		}
		SC_Value = SC_Value >> 1;
	}
}

bool XBeeClient::doCheckXBeeModuleConfiguration(bool correctConfiguration,
								  bool &configurationChanged,
								  const uint32_t& sh, const uint32_t& sl) {
	DPRINTSLN(DBG_DIAGNOSTIC,
		"doCheckXBeeModuleConfiguration() must be implemented by subclasses of XBeeClient");
	return false;
}

bool XBeeClient::setParameter(const char* param, uint8_t value[], uint8_t valueSize,
		const uint32_t& sh, const uint32_t& sl, uint8_t maxRetries) {
	bool result, doNotRetry;
	do {
		DPRINTS(DBG_RETRIES,"Attempting set of ");
		DPRINTLN(DBG_RETRIES,param);
		result=setParameterOnce(param, value, valueSize, doNotRetry,sh,sl);
	} while (!result && !doNotRetry && maxRetries--);
	return result;
}

bool XBeeClient::setParameter(const char* param, uint64_t value,
		const uint32_t& sh, const uint32_t& sl) {
	DPRINTSLN(DBG_SET,"SetParameter/uint64_t");
	return setIntegerParameter(param, value, (uint8_t) 8, sh,sl);
}

bool XBeeClient::setParameter(const char* param, uint32_t value,
		const uint32_t& sh, const uint32_t& sl) {
	DPRINTSLN(DBG_SET,"SetParameter/uint32_t");
	return setIntegerParameter(param, value, (uint8_t) 4, sh, sl);
}

bool XBeeClient::setParameter(const char* param, uint16_t value,
		const uint32_t& sh, const uint32_t& sl) {
	return setIntegerParameter(param, value, (uint8_t) 2,sh,sl);
}

bool XBeeClient::setParameter(const char* param, uint8_t value,
		const uint32_t& sh, const uint32_t& sl) {
	return setIntegerParameter(param, value, (uint8_t) 1, sh, sl);
}

bool XBeeClient::setParameter(const char* param, const char* value,
		const uint32_t& sh, const uint32_t& sl) {
	return setParameter(param, (uint8_t*) value, strlen(value),sh,sl);
}

bool XBeeClient::setParameter(const char* param, bool value,
		const uint32_t& sh, const uint32_t& sl) {
	uint8_t hexValue = (value ? 1 : 0);
	uint8_t hexSize=1;
	return setParameter(param, (uint8_t*) &hexValue, hexSize,sh,sl);
}

#undef NON_OPERATIONAL_METHOD
#ifdef NON_OPERATIONAL_METHOD // See comment in method doc block.
bool XBeeClient::restoreDefaultConfiguration() {
	// Restoring will possibly change the serial baud rate (default is 9600).
	// We'll need to change the baud rate to 9600 to reconfigure it.
	uint8_t setting;
	long baudRate;
	if (!getXBeeSerialBaudRate(baudRate, setting)) {
		DPRINTSLN(DBG_DIAGNOSTIC, "Could not get baud rate before restoring configuration");
		return false;
	}
	DPRINTS(DBG_RESTORE, "current baud rate=");
	DPRINTLN(DBG_RESTORE, baudRate);

#undef TMP_BAUD // This change of baud rate works. Why does the restore fail??
#ifdef TMP_BAUD
	uint8_t val[1]={0x3};
	if (!setParameter("BD", val,1)) {
			DPRINTSLN(DBG_DIAGNOSTIC, " TMP: Could change baudrate to 9600");
			return false;
		}
#else
	if (!sendAT_Command("RE")) {
		DPRINTSLN(DBG_DIAGNOSTIC, "Could not restore configuration");
		return false;
	}
#endif
Serial << "WAITING AFTER RESTORE/TMP_BAUD.... " << ENDL;
delay(5000);
	// Now baud rate is set to 9600.
#ifdef ARDUINO_AVR_UNO
	if (swSerialStream) {
		swSerialStream->end();
		swSerialStream->begin(9600);
	} else {
		hwSerialStream->end();
		hwSerialStream->begin(9600);
		xbee.begin(*swSerialStream);
	}
#else
	hwSerialStream->end();
	hwSerialStream->begin(9600);
	while (!*hwSerialStream); // No effect
	DPRINTSLN(DBG_RESTORE, "HW serial now ready at 9600 baud");
	xbee.begin(*hwSerialStream); // No effect (just stores a ref to the Serial object).
#endif
	if (!setParameter("BD", &setting, 1)) {
		DPRINTS(DBG_DIAGNOSTIC, "Could not change baudrate back to");
		DPRINTLN(DBG_DIAGNOSTIC, setting);
		return false;
	}
#ifdef ARDUINO_AVR_UNO
	if (swSerialStream) {
		swSerialStream->end();
		swSerialStream->begin(baudRate);
	} else {
		hwSerialStream->end();
		hwSerialStream->begin(baudRate);
	}
#else
	hwSerialStream->end();
	hwSerialStream->begin(baudRate);
#endif
	return true;

}
#endif

bool XBeeClient::softwareReset() {
	if (sendAT_Command("FR")) {
		delay(200);
		return true;}
	else return false;
};

bool XBeeClient::getXBeeSerialBaudRate(long &baudRate, uint8_t &xbeeSetting) {
	uint64_t value;
	if (!queryParameter("BD", value)) {
		return -1;
	}
	switch(value) {
	case 0x1: baudRate=2400; break;
	case 0x2: baudRate=4800; break;
	case 0x3: baudRate=9600; break;
	case 0x4: baudRate=19200; break;
	case 0x5: baudRate=38400; break;
	case 0x6: baudRate=57600; break;
	case 0x7: baudRate=115200; break;
	case 0x8: baudRate=230400; break;
	case 0x9: baudRate=460800; break;
	case 0xA: baudRate=921600; break;
	default:
		DPRINTLN(DBG_DIAGNOSTIC, "Invalid value for 'BD': ");
		DPRINTLN(DBG_DIAGNOSTIC, (uint32_t) value);
		return false;
	}
	xbeeSetting = (uint8_t) value;
	return true;
}

bool XBeeClient::queryIntegerParameter(const char* param, uint64_t &value, uint8_t numBytes,
			const uint32_t& sh, const uint32_t& sl) {
	uint8_t hexVal[8];
	uint8_t hexValSize=numBytes;
#if (DBG_QUERY==1)
	Serial << "QueryIntegerParameter param="<< param << ENDL;
#endif
	bool result=queryParameter(param, hexVal, hexValSize, sh, sl);
	if (result) {
		// MSB is byte 0
		value=0;
		for (int i = 0; i<hexValSize; i++) {
			value <<= 8;
			value+=hexVal[i];
		}
#if (DBG_QUERY==1)
	Serial << "QueryIntegerParameter val="<< value << ", numBytes=" << numBytes << ENDL;
#endif
	}
	return result;
}

bool XBeeClient::setIntegerParameter(const char* param, uint64_t value, uint8_t numBytes,
		const uint32_t& sh, const uint32_t& sl) {
	uint8_t hexVal[8];
#if (DBG_SET==1)
	Serial << "SetIntegerParameter param="<< param << ", numBytes=" << numBytes;
	Serial << " val=" << value << ENDL;
#endif
	// MSB is byte 0 ! Don't take any chance guessing whether we are running little-endian or big-endian
	for (int i=numBytes-1; i>=0; i--){
		hexVal[i] = value & 0xFF;
		value >>= 8;
	}
	return setParameter(param, hexVal, numBytes, sh, sl);
}

bool XBeeClient::queryParameterOnce(const char* param,
		uint8_t value[], uint8_t &valueSize,
		bool &doNotRetry, const uint32_t & sh, const uint32_t& sl) {
	DPRINTS(DBG_QUERY, "QueryParameterOnce param=");
	DPRINTLN(DBG_QUERY, param);
	DPRINTS(DBG_QUERY, "valueSize=");
	DPRINTLN(DBG_QUERY, valueSize);
	if(param[2]!= '\0') {
		DPRINTS(DBG_DIAGNOSTIC, "Error: invalid XBee parameter (aborted):");
		DPRINTLN(DBG_DIAGNOSTIC, param);
		return false;
	}

	if ((sh==0) && (sl==0)) {
		return queryLocalParameterOnce(param, value , valueSize,
				doNotRetry);
	}
	else {
		XBeeAddress64 remote(sh, sl);
		return queryRemoteParameterOnce(param, value, valueSize,
				doNotRetry, remote);
	}
}

#if (DBG_DIAGNOSTIC==1)
static void printResponseStatus(const char* param, uint8_t status) {
	Serial << param << F(": Error code: 0x");
	switch (status) {
	case 0: Serial << F("0 (OK)");	break;
	case 1: Serial << F("1 (Error)");	break;
	case 2: Serial << F("2 (Invalid cmd)");	 break;
	case 3: Serial << F("3 (Invalid param.)");	break;
	case 4: Serial << F("4 (Transm. failure)");	break;
	default:
		Serial << status << F(" (Unknown)");
	}
	Serial << ENDL;
}
#endif

bool XBeeClient::queryLocalParameterOnce(const char* param,
		uint8_t value[], uint8_t &valueSize,
		bool &doNotRetry) {
	DPRINTS(DBG_QUERY, "QueryLocalParameterOnce param=");
	DPRINTLN(DBG_QUERY, param);
	DPRINTS(DBG_QUERY, "valueSize=");
	DPRINTLN(DBG_QUERY, valueSize);
	if(param[2]!= '\0') {
		DPRINTS(DBG_DIAGNOSTIC, "Error: invalid XBee parameter (aborted):");
		DPRINTLN(DBG_DIAGNOSTIC, param);
		return false;
	}
	AtCommandRequest req((uint8_t*) param);
	AtCommandResponse response;
	bool result=false, responseReceived=false;
	doNotRetry=false; // Only forbid retry in case it is hopeless.
	xbee.send(req);
	// wait for AT_ResponseDelay msec max for answer, ignore whatever is received in the meantime
	elapsedMillis elapsed=0;
	while (!responseReceived && xbee.readPacket(AT_ResponseDelay-elapsed)) {
		// got a response! Should be an AT command response
		if (xbee.getResponse().getApiId() == AT_COMMAND_RESPONSE) {
			xbee.getResponse().getAtCommandResponse(response);
			responseReceived=true;
			if (response.isOk()) {
				DPRINTSLN(DBG_QUERY, "Command successful");
				if (response.getValueLength() > 0) {
					DPRINTS(DBG_QUERY, "value length= ");
					DPRINTLN(DBG_QUERY, response.getValueLength());
					DPRINTS(DBG_QUERY, "response value [](HEX): ");
					for (int i = 0; i < response.getValueLength(); i++) {
						DPRINT(DBG_QUERY, i);
						DPRINTS(DBG_QUERY, "=");
						DPRINT(DBG_QUERY, (response.getValue()[i]<0x10 ? "0": ""));
						DPRINT(DBG_QUERY,response.getValue()[i], HEX);
						DPRINTS(DBG_QUERY, " ");
					}
					DPRINTSLN(DBG_QUERY, " ");
					DPRINTS(DBG_QUERY, "response value (HEX): ");
					for (int i = 0; i < response.getValueLength(); i++) {
						DPRINT(DBG_QUERY, (response.getValue()[i]<0x10 ? "0": ""));
						DPRINT(DBG_QUERY,response.getValue()[i], HEX);
					}
					DPRINTSLN(DBG_QUERY,"");

					if (response.getValueLength() > valueSize) {
						DPRINTS(DBG_DIAGNOSTIC,"XBeeClient::QueryParameterOnce: value too large! Param=");
						DPRINT(DBG_DIAGNOSTIC, param);
						DPRINTS(DBG_DIAGNOSTIC, ", bufferSize=")
								DPRINT(DBG_DIAGNOSTIC,valueSize);
						DPRINTS(DBG_DIAGNOSTIC,", valueSize=")
						DPRINTLN(DBG_DIAGNOSTIC,response.getValueLength());
						doNotRetry=true;
					} else {
						valueSize=response.getValueLength();
						for (int i = 0; i < valueSize; i++) {
							value[i]=response.getValue()[i];
						}

						result=true;
					}
				} else {
					// Null-length response: we assume it is an error unless the
					// valueSize is 0.
					if (valueSize >0) {
						DPRINTSLN(DBG_DIAGNOSTIC, "Error: null-length response received.");
					} else result=true;
				}
			}
			else {   // response nok.
#if (DBG_DIAGNOSTIC==1)
				printResponseStatus(param, response.getStatus());
#endif
				doNotRetry=(response.getStatus() >1);
			}
		}
		else if (xbee.getResponse().getApiId() == ExtendedTransmitStatus) {
			DPRINTSLN(DBG_DIAGNOSTIC,"Expected AT response but got ExtendedTransmitStatus ");
			DPRINTSLN(DBG_DIAGNOSTIC,"TODO extract delivery status from payload byte 5");
			DPRINTSLN(DBG_DIAGNOSTIC,"TODO extract discovery status from payload byte 6");
			DPRINTSLN(DBG_DIAGNOSTIC," (ignored)");
		}
		else {
			DPRINTS(DBG_DIAGNOSTIC,"Expected AT response but got ");
			DPRINT(DBG_DIAGNOSTIC,xbee.getResponse().getApiId(), HEX);
			DPRINTSLN(DBG_DIAGNOSTIC," (ignored)");
		}
	}
	if (!result) {
		// AT command failed
		if (responseReceived) {
			if (xbee.getResponse().isError()) {
				DPRINTS(DBG_DIAGNOSTIC,"Error reading packet. Error code: ");
				DPRINTLN(DBG_DIAGNOSTIC,xbee.getResponse().getErrorCode());
			} else {
				DPRINTSLN(DBG_DIAGNOSTIC,"Invalid response.");
			}
		}
		else {
			DPRINTSLN(DBG_DIAGNOSTIC,"No response from radio");
		}
	}
	return result;
}


bool XBeeClient::queryRemoteParameterOnce(const char* param,
		uint8_t value[], uint8_t &valueSize,
		bool &doNotRetry, XBeeAddress64 &remote) {
#if (DBG_REMOTE_QUERY==1)
	Serial<< "QueryRemoteParameterOnce param=" << param <<  ", valueSize=" <<valueSize << ENDL;
	Serial<< "   doNotRetry=" << doNotRetry << ", remote=";
	PrintHexWithLeadingZeroes(Serial, remote.getMsb());
	Serial << '-';
	PrintHexWithLeadingZeroes(Serial, remote.getLsb());
	Serial << ENDL;
#endif
	if(param[2]!= '\0') {
		DPRINTS(DBG_DIAGNOSTIC, "Error: invalid XBee parameter (aborted):");
		DPRINTLN(DBG_DIAGNOSTIC, param);
		return false;
	}
	RemoteAtCommandRequest req(remote, (uint8_t*) param);
	RemoteAtCommandResponse response;
	bool result=false, responseReceived=false;
	doNotRetry=false; // Only forbid retry in case it is hopeless.
	xbee.send(req);
	// wait for RemoteAT_ResponseDelay msec max for answer,
	// ignore whatever is received in the meantime
	elapsedMillis elapsed=0;
	while (!responseReceived && xbee.readPacket(RemoteAT_ResponseDelay-elapsed)) {
		// got a response! Should be an AT command response
		if (xbee.getResponse().getApiId() == REMOTE_AT_COMMAND_RESPONSE) {
			xbee.getResponse().getAtCommandResponse(response);
			responseReceived=true;
			if (response.isOk()) {
				DPRINTSLN(DBG_REMOTE_QUERY, "Command successful");
				if (response.getValueLength() > 0) {
					DPRINTS(DBG_REMOTE_QUERY, "value length= ");
					DPRINTLN(DBG_REMOTE_QUERY, response.getValueLength());
					DPRINTS(DBG_REMOTE_QUERY, "response value [](HEX): ");
					for (int i = 0; i < response.getValueLength(); i++) {
						DPRINT(DBG_REMOTE_QUERY, i);
						DPRINTS(DBG_REMOTE_QUERY, "=");
						DPRINT(DBG_REMOTE_QUERY, (response.getValue()[i]<0x10 ? "0": ""));
						DPRINT(DBG_REMOTE_QUERY,response.getValue()[i], HEX);
						DPRINTS(DBG_REMOTE_QUERY, " ");
					}
					DPRINTSLN(DBG_REMOTE_QUERY, " ");
					DPRINTS(DBG_REMOTE_QUERY, "response value (HEX): ");
					for (int i = 0; i < response.getValueLength(); i++) {
						DPRINT(DBG_REMOTE_QUERY, (response.getValue()[i]<0x10 ? "0": ""));
						DPRINT(DBG_REMOTE_QUERY,response.getValue()[i], HEX);
					}
					DPRINTSLN(DBG_REMOTE_QUERY,"");

					if (response.getValueLength() > valueSize) {
						DPRINTS(DBG_DIAGNOSTIC,"XBeeClient::QueryParameterOnce: value too large! Param=");
						DPRINT(DBG_DIAGNOSTIC, param);
						DPRINTS(DBG_DIAGNOSTIC, ", bufferSize=")
								DPRINT(DBG_DIAGNOSTIC,valueSize);
						DPRINTS(DBG_DIAGNOSTIC,", valueSize=")
						DPRINTLN(DBG_DIAGNOSTIC,response.getValueLength());
						doNotRetry=true;
					} else {
						valueSize=response.getValueLength();
						for (int i = 0; i < valueSize; i++) {
							value[i]=response.getValue()[i];
						}

						result=true;
					}
				} else {
					// Null-length response: we assume it is an error unless the
					// valueSize is 0.
					if (valueSize >0) {
						DPRINTSLN(DBG_DIAGNOSTIC, "Error: null-length response received.");
					} else result=true;
				}
			}
			else {
#if (DBG_DIAGNOSTIC==1)
				printResponseStatus(param, response.getStatus());
#endif
				doNotRetry=(response.getStatus() >1);
			}
		}
		else if (xbee.getResponse().getApiId() == ExtendedTransmitStatus) {
			DPRINTSLN(DBG_DIAGNOSTIC,"Expected Remote AT response but got ExtendedTransmitStatus ");
			DPRINTSLN(DBG_DIAGNOSTIC,"TODO extract delivery status from payload byte 5");
			DPRINTSLN(DBG_DIAGNOSTIC,"TODO extract discovery status from payload byte 6");
			DPRINTSLN(DBG_DIAGNOSTIC," (ignored)");
		}
		else {
			DPRINTS(DBG_DIAGNOSTIC,"Expected Remote AT response but got ");
			DPRINT(DBG_DIAGNOSTIC,xbee.getResponse().getApiId(), HEX);
			DPRINTSLN(DBG_DIAGNOSTIC," (ignored)");
		}
	}
	if (!result) {
		// AT command failed
		if (responseReceived) {
			if (xbee.getResponse().isError()) {
				DPRINTS(DBG_DIAGNOSTIC,"Error reading packet. Error code: ");
				DPRINTLN(DBG_DIAGNOSTIC,xbee.getResponse().getErrorCode());
			} else {
				DPRINTSLN(DBG_DIAGNOSTIC,"Invalid response.");
			}
		}
		else {
			DPRINTSLN(DBG_DIAGNOSTIC,"No response from radio");
		}
	}
	return result;
}

bool XBeeClient::setParameterOnce(const char* param,
		uint8_t value[], uint8_t valueSize,
		bool &doNotRetry, const uint32_t& sh, const uint32_t& sl) {
	DPRINTS(DBG_QUERY, "SetParameterOnce param=");
	DPRINTLN(DBG_QUERY, param);
	DPRINTS(DBG_QUERY, "valueSize=");
	DPRINTLN(DBG_QUERY, valueSize);
	if(param[2]!= '\0') {
		DPRINTS(DBG_DIAGNOSTIC, "Error: invalid XBee parameter (aborted):");
		DPRINTLN(DBG_DIAGNOSTIC, param);
		return false;
	}

	if ((sh==0) && (sl==0)) {
		return setLocalParameterOnce(param, value , valueSize,
				doNotRetry);
	}
	else {
		XBeeAddress64 remote(sh, sl);
		return setRemoteParameterOnce(param, value, valueSize,
				doNotRetry, remote);
	}
}


bool XBeeClient::setLocalParameterOnce(const char* param,
		uint8_t value[], uint8_t valueSize, bool &doNotRetry) {
	DPRINTS(DBG_SET, "SetLocalParameterOnce. Setting ");
	DPRINT(DBG_SET, param);
	DPRINTS(DBG_SET, " to value ");
	for (int i = 0; i < valueSize; i++) {
		DPRINT(DBG_SET, i);
		DPRINT(DBG_SET, ": 0x");
		DPRINT(DBG_SET, (value[i]<0x10 ? "0": ""));
		DPRINT(DBG_SET,value[i], HEX);
		DPRINTS(DBG_SET," ");
	}
	DPRINTSLN(DBG_SET,"");
	if(param[2]!= '\0') {
		DPRINTS(DBG_DIAGNOSTIC, "Error: invalid XBee parameter (aborted):");
		DPRINTLN(DBG_DIAGNOSTIC, param);
		return false;
	}
	AtCommandRequest req((uint8_t*) param, value, valueSize);
	AtCommandResponse response;
	bool result=false, responseReceived=false;
	doNotRetry=false; // forbid retries only when hopeless.
	xbee.send(req);
	// wait for AT_ResponseDelay msec max for answer, ignore whatever is received in the meantime
	elapsedMillis elapsed=0;
	while (!responseReceived && xbee.readPacket(AT_ResponseDelay-elapsed)) {
		// got a response! Should be an AT command response
		if (xbee.getResponse().getApiId() == AT_COMMAND_RESPONSE) {
			xbee.getResponse().getAtCommandResponse(response);
			responseReceived=true;
			if (response.isOk()) {
				DPRINTSLN(DBG_SET, "Command successful");
				result=true;
			}
			else {
#if (DBG_DIAGNOSTIC==1)
				printResponseStatus(param, response.getStatus());
#endif
				doNotRetry=(response.getStatus() >1);			}
		}
		else if (xbee.getResponse().getApiId() == ExtendedTransmitStatus) {
			DPRINTSLN(DBG_DIAGNOSTIC,"Expected AT response but got ExtendedTransmitStatus ");
			DPRINTSLN(DBG_DIAGNOSTIC,"TODO extract delivery status from payload byte 5");
			DPRINTSLN(DBG_DIAGNOSTIC,"TODO extract discovery status from payload byte 6");
			DPRINTSLN(DBG_DIAGNOSTIC," (ignored)");
		}
		else {
			DPRINTS(DBG_DIAGNOSTIC,"Expected AT response but got 0x");
			DPRINT(DBG_DIAGNOSTIC,xbee.getResponse().getApiId(), HEX);
			DPRINTSLN(DBG_DIAGNOSTIC," (ignored)");
		}
	}
	if (!result) {
		// AT command failed
		if (xbee.getResponse().isError()) {
			DPRINTS(DBG_DIAGNOSTIC,"Error reading packet. Error code: ");
			DPRINTLN(DBG_DIAGNOSTIC,xbee.getResponse().getErrorCode());
		}
		else {
			DPRINTSLN(DBG_DIAGNOSTIC,"No response from radio");
		}
	}
	return result;
}


bool XBeeClient::setRemoteParameterOnce(const char* param,
		uint8_t value[], uint8_t valueSize,
		bool &doNotRetry, XBeeAddress64 &remote) {
	DPRINTS(DBG_SET, "SetRemoteParameterOnce. Setting ");
	DPRINT(DBG_SET, param);
	DPRINTS(DBG_SET, " to value ");
	for (int i = 0; i < valueSize; i++) {
		DPRINT(DBG_SET, i);
		DPRINT(DBG_SET, ": 0x");
		DPRINT(DBG_SET, (value[i]<0x10 ? "0": ""));
		DPRINT(DBG_SET,value[i], HEX);
		DPRINTS(DBG_SET," ");
	}
	DPRINTSLN(DBG_SET,"");
	if(param[2]!= '\0') {
		DPRINTS(DBG_DIAGNOSTIC, "Error: invalid XBee parameter (aborted):");
		DPRINTLN(DBG_DIAGNOSTIC, param);
		return false;
	}
	RemoteAtCommandRequest req(remote,(uint8_t*) param, value, valueSize);
	RemoteAtCommandResponse response;
	bool result=false, responseReceived=false;
	doNotRetry=false; // forbid retries only when hopeless.
	xbee.send(req);
	// wait for AT_ResponseDelay msec max for answer, ignore whatever is received in the meantime
	elapsedMillis elapsed=0;
	while (!responseReceived && xbee.readPacket(RemoteAT_ResponseDelay-elapsed)) {
		// got a response! Should be an AT command response
		if (xbee.getResponse().getApiId() == REMOTE_AT_COMMAND_RESPONSE) {
			xbee.getResponse().getAtCommandResponse(response);
			responseReceived=true;
			if (response.isOk()) {
				DPRINTSLN(DBG_SET, "Command successful");
				result=true;
			}
			else {
#if (DBG_DIAGNOSTIC==1)
				printResponseStatus(param, response.getStatus());
#endif
				doNotRetry=(response.getStatus() >1);			}
		}
		else if (xbee.getResponse().getApiId() == ExtendedTransmitStatus) {
			DPRINTSLN(DBG_DIAGNOSTIC,"Expected AT response but got ExtendedTransmitStatus ");
			DPRINTSLN(DBG_DIAGNOSTIC,"TODO extract delivery status from payload byte 5");
			DPRINTSLN(DBG_DIAGNOSTIC,"TODO extract discovery status from payload byte 6");
			DPRINTSLN(DBG_DIAGNOSTIC," (ignored)");
		}
		else {
			DPRINTS(DBG_DIAGNOSTIC,"Expected Remote AT response but got 0x");
			DPRINT(DBG_DIAGNOSTIC,xbee.getResponse().getApiId(), HEX);
			DPRINTSLN(DBG_DIAGNOSTIC," (ignored)");
		}
	}
	if (!result) {
		// AT command failed
		if (xbee.getResponse().isError()) {
			DPRINTS(DBG_DIAGNOSTIC,"Error reading packet. Error code: ");
			DPRINTLN(DBG_DIAGNOSTIC,xbee.getResponse().getErrorCode());
		}
		else {
			DPRINTSLN(DBG_DIAGNOSTIC,"No response from radio");
		}
	}
	return result;
}


bool XBeeClient::checkParameter(	const char* param,
									uint64_t value,
									bool updateConfiguration,
									bool &configurationChanged,
									const uint32_t& sh, const uint32_t& sl)
{
	DPRINTSLN(DBG_CHECK,"CheckParameter/uint64_t");
	uint64_t currentValue;

	if (!queryParameter(param, currentValue, sh, sl)) return false;
	if (currentValue != value) {
#if (DBG_DIAGNOSTIC==1)
		Serial << param << " is not OK: ";
		PrintHexWithLeadingZeroes(Serial,currentValue);
		Serial  <<  " instead of ";
		PrintHexWithLeadingZeroes(Serial, value);
		if (!updateConfiguration) Serial << ENDL;
#endif

		if (!updateConfiguration) return false;
		if (!setParameter(param, value, sh, sl)) {
			DPRINTLN(DBG_DIAGNOSTIC, " (update failed)");
			return false;
		}
		DPRINTLN(DBG_DIAGNOSTIC, " (updated)");
		configurationChanged=true;
	}
	else {
		DPRINT(DBG_CHECK, param);
		DPRINTSLN(DBG_CHECK, " OK");
	}
    return true;
}

bool XBeeClient::checkParameter(	const char* param,
									uint32_t value,
									bool updateConfiguration,
									bool &configurationChanged,
									const uint32_t& sh, const uint32_t& sl)
{
	DPRINTSLN(DBG_CHECK,"CheckParameter/uint32_t");
	uint32_t currentValue;

	if (!queryParameter(param, currentValue, sh,sl)) return false;
	if (currentValue != value) {
#if (DBG_DIAGNOSTIC==1)
		Serial << param << " is not OK: ";
		PrintHexWithLeadingZeroes(Serial,currentValue);
		Serial  <<  " instead of ";
		PrintHexWithLeadingZeroes(Serial, value);
		if (!updateConfiguration) Serial << ENDL;
#endif

		if (!updateConfiguration) return false;
		if (!setParameter(param, value, sh, sl)) {
			DPRINTLN(DBG_DIAGNOSTIC, " (update failed)");
			return false;
		}
		DPRINTLN(DBG_DIAGNOSTIC, " (updated)");
		configurationChanged=true;
	}
	else {
		DPRINT(DBG_CHECK, param);
		DPRINTSLN(DBG_CHECK, " OK");
	}
    return true;
}

bool XBeeClient::checkParameter(	const char* param,
									uint16_t value,
									bool updateConfiguration,
									bool &configurationChanged,
									const uint32_t& sh, const uint32_t& sl)
{
	DPRINTSLN(DBG_CHECK,"CheckParameter/uint16_t");
	uint16_t currentValue;

	if (!queryParameter(param, currentValue, sh,sl)) return false;
	if (currentValue != value) {
#if (DBG_DIAGNOSTIC==1)
		Serial << param << " is not OK: ";
		PrintHexWithLeadingZeroes(Serial,currentValue);
		Serial  <<  " instead of ";
		PrintHexWithLeadingZeroes(Serial, value);
		if (!updateConfiguration) Serial << ENDL;
#endif

		if (!updateConfiguration) return false;
		if (!setParameter(param, value, sh,sl)) {
			DPRINTLN(DBG_DIAGNOSTIC, " (update failed)");
			return false;
		}
		DPRINTLN(DBG_DIAGNOSTIC, " (updated)");
		configurationChanged=true;
	}
	else {
		DPRINT(DBG_CHECK, param);
		DPRINTSLN(DBG_CHECK, " OK");
	}
    return true;
}

bool XBeeClient::checkParameter(	const char* param,
									uint8_t value,
									bool updateConfiguration,
									bool &configurationChanged,
									const uint32_t& sh, const uint32_t& sl)
{
#if (DBG_CHECK==1)
	Serial << "CheckParameter/uint8_t, param=" << param << ", value=" << value << ENDL;
	Serial << "sh-sl: "; PrintHexWithLeadingZeroes(Serial, sh);
	Serial << " - : "; PrintHexWithLeadingZeroes(Serial, sl);
	Serial << ENDL;
#endif
	uint8_t currentValue;

	if (!queryParameter(param, currentValue,sh,sl)) return false;
	if (currentValue != value) {
#if (DBG_DIAGNOSTIC==1)
		Serial << param << " is not OK: ";
		PrintHexWithLeadingZeroes(Serial, currentValue);
		Serial  <<  " instead of ";
		PrintHexWithLeadingZeroes(Serial, value);
		if (!updateConfiguration) Serial << ENDL;
#endif

		if (!updateConfiguration) return false;
		if (!setParameter(param, value, sh,sl)) {
			DPRINTLN(DBG_DIAGNOSTIC, " (update failed)");
			return false;
		}
		DPRINTLN(DBG_DIAGNOSTIC, " (updated)");
		configurationChanged=true;
	}
	else {
		DPRINT(DBG_CHECK, param);
		DPRINTSLN(DBG_CHECK, " OK");
	}
    return true;
}

bool XBeeClient::checkParameter(	const char* param,
									const char* value,
									bool updateConfiguration,
									bool &configurationChanged,
									const uint32_t& sh, const uint32_t& sl)
{
#if (DBG_CHECK==1)
	Serial << "CheckParameter/const char*, param=" << param << ", value=" << value << ENDL;
	Serial << "sh-sl: "; PrintHexWithLeadingZeroes(Serial, sh);
	Serial << " - : "; PrintHexWithLeadingZeroes(Serial, sl);
	Serial << ENDL;
#endif
	char currentValue[MaxStringValueLength];

	if (!queryParameter(param, currentValue,MaxStringValueLength, sh, sl)) return false;
	if (strcmp(currentValue,value)) {
#if (DBG_DIAGNOSTIC==1)
		Serial << param << " is not OK: '";
		Serial  << currentValue <<  "' instead of '" << value << "'";
		if (!updateConfiguration) Serial << ENDL;
#endif

		if (!updateConfiguration) return false;
		if (!setParameter(param, value, sh,sl)) {
			DPRINTLN(DBG_DIAGNOSTIC, " (update failed)");
			return false;
		}
		DPRINTLN(DBG_DIAGNOSTIC, " (updated)");
		configurationChanged=true;
	}
	else {
		DPRINT(DBG_CHECK, param);
		DPRINTSLN(DBG_CHECK, " OK");
	}
    return true;
}

bool XBeeClient::checkParameter(	const char* param,
									bool value,
									bool updateConfiguration,
									bool &configurationChanged,
									const uint32_t& sh, const uint32_t& sl)
{
	DPRINTSLN(DBG_CHECK,"CheckParameter/bool");
	bool currentValue;

	if (!queryParameter(param, currentValue, sh,sl)) return false;
	if (currentValue != value) {
#if (DBG_DIAGNOSTIC==1)
		Serial << param << " is not OK: 0x";
		Serial.print(currentValue, HEX);
		Serial  <<  " instead of ";
		Serial.print(value, HEX);
		if (!updateConfiguration) Serial << ENDL;
#endif

		if (!updateConfiguration) return false;
		if (!setParameter(param, value, sh, sl)) {
			DPRINTLN(DBG_DIAGNOSTIC, " (update failed)");
			return false;
		}
		DPRINTLN(DBG_DIAGNOSTIC, " (updated)");
		configurationChanged=true;
	}
	else {
		DPRINT(DBG_CHECK, param);
		DPRINTSLN(DBG_CHECK, " OK");
	}
    return true;
}

bool XBeeClient::checkDevicetype( XBeeClient::DeviceType value,
		 	 	 	 	bool correctConfiguration,
						bool &configurationChanged,
						const uint32_t& sh, const uint32_t& sl)
{
	DeviceType currentType=getDeviceType(sh, sl);
	if (currentType==value) {
		DPRINTSLN(DBG_CHECK, "Device type OK");
		return true;
	}
#if (DBG_DIAGNOSTIC==1)
	Serial << "Device type is not OK: " << getLabel(currentType);
	Serial  <<  " instead of "<< getLabel(value);
	if (!correctConfiguration) Serial << ENDL;
#endif
	if (correctConfiguration) {
		if (setDeviceType(value, sh, sl)) {
			DPRINTSLN(DBG_DIAGNOSTIC, " (updated)");
			configurationChanged=true;
			return true;
		} else return false;
	} else return false;
}

XBeeClient::DeviceType XBeeClient::getDeviceType(const uint32_t& sh, const uint32_t& sl) {
	bool CE_Value, SM_Value;
	if (!queryParameter("CE", CE_Value, sh, sl)) return DeviceType::Unknown;
	if (!queryParameter("SM", SM_Value, sh, sl)) return DeviceType::Unknown;
	if (CE_Value) return DeviceType::Coordinator;
	else {
		if (SM_Value) return DeviceType::EndDevice;
		else return DeviceType::Router;
	}
}

bool XBeeClient::setDeviceType(XBeeClient::DeviceType type,
		const uint32_t& sh,const uint32_t& sl) {
	bool result;
	switch(type) {
	case DeviceType::Coordinator:
		// Set CM to 0 first!
		result = setParameter("CM", false, sh, sl);
		if (!setParameter("CE", true, sh, sl)) result=false;
		break;
	case DeviceType::Router:
		// Set CE to 0 first!
		result = setParameter("CE", false, sh, sl);
		if (!setParameter("CM", false, sh, sl)) result=false;
		break;
	case DeviceType::EndDevice:
		// Set CE to 0 first!
		// This set the sleeping mode to 1 (pin sleep enabled).
		result = setParameter("CE", false, sh, sl);
		if (!setParameter("CM", true, sh, sl)) result=false;
		break;
	case DeviceType::Unknown:
	default:
		result=false;
		DPRINTS(DBG_SET, "Invalid device type received: ");
		DPRINTS(DBG_DIAGNOSTIC, (uint8_t) type);

	}
	return result;
}
const char* XBeeClient::getLabel(XBeeClient::DeviceType value) {
	switch(value) {
	case DeviceType::Coordinator:
		return "Coordinator";
		break;
	case DeviceType::Router:
		return "Router";
		break;
	case DeviceType::EndDevice:
		return "EndDevice";
		break;
	case DeviceType::Unknown:
		return "Unknown";
		break;
	default:
		return "** Invalid device type **";
	}
}

#endif // API_MODE
