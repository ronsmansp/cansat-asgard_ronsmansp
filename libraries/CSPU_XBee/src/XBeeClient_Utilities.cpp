/*
 * XBeeClient_Utilities.cpp
 *
 * A couple of utility methods not required for operation. They are located in a separated
 * compilation unit to avoid unnecessary inclusion in the executable.
 *
 */
// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "CansatConfig.h"
#include "XBeeClient.h"
#include "XBee.h"

#ifdef RF_ACTIVATE_API_MODE

#define DBG_DIAGNOSTIC 1
#define DBG_ND	  	   0

void XBeeClient::printFrame(uint8_t* data, uint8_t dataSize)
{
  Serial << "--- Frame content (size=" << dataSize << ") ---" << ENDL;
  for (int i = 0; i< dataSize ; i++)
  {
	  Serial.print(data[i], HEX);
	  Serial << " ";
	  if (((i+1)%10) == 0) Serial << ENDL;
	  else if (((i+1)%5) == 0) Serial << "  ";
  }
  if ((dataSize % 10) != 0) {
	  Serial << ENDL;
  }
  Serial << "--- End of frame content ---" << ENDL;
}

void XBeeClient::printString(uint8_t* data, uint8_t dataSize) const
{
  uint8_t type = data[0];
  const char* s = (const char*) (data + 2);
  Serial << "String message: type=" << type << ", '" << s << "'" << ENDL;
  Serial << "length=" << strlen(s) << ", size=" << dataSize;
  if (strlen(s) + 3 != (size_t) (dataSize)) {
	Serial << ENDL << "*** Error: inconsistent size (" << strlen(s) + 3
		   << " vs " << dataSize  << ")" << ENDL;
	// byte 0 = type, 1=unused, last byte = \0.
  }
  if ((strlen(s) > 0) && (s[strlen(s) - 1] == '\n')) {
	Serial << "(last character is an ENDL)";
  }
  Serial << ENDL;
}

void XBeeClient::printND_Response(const uint8_t* response) {
  Serial << "Discovered one node: ";
  uint16_t ui16=(response[0] << 8) | (response[1]);
  response += 2;
  Serial << " MY="; PrintHexWithLeadingZeroes(Serial, ui16);
  uint32_t ui32=(response[0] << 24) | (response[1] << 16)| (response[2] << 8)| response[3];
  response+=4;
  Serial << " SH="; PrintHexWithLeadingZeroes(Serial, ui32);
  ui32=(response[0] << 24) | (response[1] << 16)| (response[2] << 8)| response[3];
  response+=4;
  Serial << " SL="; PrintHexWithLeadingZeroes(Serial, ui32);
  // Specification puts an RSSI byte here, but it is obviously not present?
  // Serial << " RSSI: -" << *response << " dBm";
  // response++;
  Serial << " NI: '" << (char *) response << "'" << ENDL;
}

void XBeeClient::printDiscoveredNodes() {
  // Wait for more than one AT Cmd Response (one per discovered node)
  // and use time-out provided by NT.
  uint16_t ntValue, myAddress;
  uint64_t panID;
  constexpr uint8_t MaxNameSize=25;
  char myName[MaxNameSize];
  if (!queryParameter("NT", ntValue)) {
    Serial << "*** Error sending 'NT' command." << ENDL;
    return;
  }
  if (!queryParameter("NI", myName, MaxNameSize)) {
    Serial << "*** Error sending 'NI' command." << ENDL;
    return;
  }
  if (!queryParameter("ID", panID)) {
    Serial << "*** Error sending 'ID' command." << ENDL;
    return;
  }
  if (!queryParameter("MY", myAddress)) {
    Serial << "*** Error sending 'MY' command." << ENDL;
    return;
  }

  Serial << "Sending ND command from '" << myName << "' (MY=";
  PrintHexWithLeadingZeroes(Serial, myAddress);
  Serial << ") on PAN ";
  PrintHexWithLeadingZeroes(Serial, panID);
  Serial << ", with " << ntValue * 100 << " msec time-out..." << ENDL;

#undef NEW
#ifdef NEW
  /* D'après la doc on aurait dû recevoir un Route Record 0xA1 en réponse
   * à un ND avec un NI. La détection fonctionne mais on ne reçoit pas
   * la "préface". Je suppose que ce n'est qu'en mode transparent.
   * Ceci n'a donc aucun intérêt pour obtenir un route record.
   */
  unsigned char destName[]="05_SUB_2_A";
  Serial << "QAAA ND with payload '05_SUB_2_A', " << strlen("05_SUB_2A") <<ENDL;
  AtCommandRequest req( (uint8_t*) "ND", destName, (uint8_t) strlen("05_SUB_2_A"));
#else
  AtCommandRequest req( (uint8_t*) "ND");
#endif
  AtCommandResponse response;
  bool responseReceived = false;
  xbee.send(req);
  // wait for AT_ResponseDelay msec max for answer, ignore whatever is received in the meantime
  elapsedMillis elapsed = 0;
  while (xbee.readPacket((100 * ntValue) - elapsed)) {
    // got a response! Should be an AT command response
    if (xbee.getResponse().getApiId() == AT_COMMAND_RESPONSE) {
      xbee.getResponse().getAtCommandResponse(response);
      responseReceived = true;
      if (response.isOk()) {
        DPRINTSLN(DBG_ND, "Command successful");
        if (response.getValueLength() > 0) {
          DPRINTS(DBG_ND, "value length= ");
          DPRINTLN(DBG_ND, response.getValueLength());
          DPRINTS(DBG_ND, "response value [](HEX): ");
          for (int i = 0; i < response.getValueLength(); i++) {
            DPRINT(DBG_ND, i);
            DPRINTS(DBG_ND, "=");
            DPRINT(DBG_ND, (response.getValue()[i] < 0x10 ? "0" : ""));
            DPRINT(DBG_ND, response.getValue()[i], HEX);
            DPRINTS(DBG_ND, " ");
          }
          DPRINTSLN(DBG_ND, " ");
          DPRINTS(DBG_ND, "response value (HEX): ");
          for (int i = 0; i < response.getValueLength(); i++) {
            DPRINT(DBG_ND, (response.getValue()[i] < 0x10 ? "0" : ""));
            DPRINT(DBG_ND, response.getValue()[i], HEX);
          }
          DPRINTSLN(DBG_ND, "");

          printND_Response(response.getValue());
        } else {
          // Null-length response: we assume it is an error
          DPRINTSLN(DBG_DIAGNOSTIC, "Error: null-length response received.");
        }  // value length > 0
      } // response OK
      else {
#ifdef DBG_DIAGNOSTIC
        Serial << "ND" << F(": Error code: 0x");
        switch (response.getStatus()) {
          case 0: Serial << F("0 (OK)");	break;
          case 1: Serial << F("1 (Error)");	break;
          case 2: Serial << F("2 (Invalid cmd)");	 break;
          case 3: Serial << F("3 (Invalid param.)");	break;
          default:
            Serial << response.getStatus() << F("Unknown");
        }
#endif
        DPRINTLN(DBG_DIAGNOSTIC, response.getStatus(), HEX);
      } // response NOK
    } // AT response
    else {
      DPRINTS(DBG_DIAGNOSTIC, "Expected AT response but got ");
      DPRINT(DBG_DIAGNOSTIC, xbee.getResponse().getApiId(), HEX);
      DPRINTSLN(DBG_DIAGNOSTIC, " (ignored)");
    } // not AT response
  } // while readPacket()

  // AT command failed
  if (responseReceived) {
    if (xbee.getResponse().isError()) {
      DPRINTS(DBG_DIAGNOSTIC, "Error reading packet. Error code: ");
      DPRINTLN(DBG_DIAGNOSTIC, xbee.getResponse().getErrorCode());
    }
  }
  else {
    Serial << "No node detected (no response from XBee module)" << ENDL;
  }

  Serial << "Node detection over." << ENDL;
}
#endif


