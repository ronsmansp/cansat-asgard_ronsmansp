/*
    Test for the XBeeClient methods used to manage AT commands:
     - query parameters
     - change parameters settings.
     NB: This program only test local AT Commands. The XBeeClient also
         supports remote AT commands, which are best tested using the XBee_DisplayConfig 
         and XBee_ConfigureCansatModule utilities. 

     Wiring: µC to XBee module.
     Feather/ItsyBitsy
	   3.3V to VCC
	   RX   to DOUT  CONFIRMED although it is the opposite to connect to XCTU!
	   TX   to DIN   CONFIRMED although it is the opposite to connect to XCTU!
	   GND  to GND
	 Uno: (RX-TX pins can be modified with constants below)
	   3.3V    to VCC
	   RX=D9   to DOUT  CONFIRMED although it is the opposite to connect to XCTU!
	   TX=D11  to DIN   CONFIRMED although it is the opposite to connect to XCTU!
	   GND     to GND

*/
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#define DBG 1

#include "CansatConfig.h"
#include "XBeeClient.h"

#undef INCLUDE_RESTORATION_TEST
// Define to include the test of the "Restore Factory Settings" function.
// Warning, method RestoreFactoryConfiguration

#ifdef ARDUINO_ARCH_SAMD
HardwareSerial &RF = Serial1;
#else
#error This test is too large to run on an Arduino UNO board.
#   include "SoftwareSerial.h"
static constexpr int RF_RxPinOnUNO = 9;
static constexpr int RF_TxPinOnUNO = 11;
SoftwareSerial RF(RF_RxPinOnUNO, RF_TxPinOnUNO);
#endif

#ifndef RF_ACTIVATE_API_MODE
#  error "This program only works if RF_ACTIVATE_API_MODE is defined in CansatConfig.h"
#endif

XBeeClient xbc(0, 0xFFFF); // Broadcast adress, but this is not relevant for this test.
uint32_t numErrors = 0;
uint32_t numManualChecks = 0;
constexpr uint8_t MaxValueSize = 20;

template<class T> void testSet(const char* cmd, T newValue)
{
  T savedValue;
  T tmpValue;
  Serial << ENDL << F("Testing SET...") << ENDL;
  Serial << F("  Setting  ") << cmd << " to new value" << ENDL;
  if (!xbc.queryParameter(cmd, savedValue)) {
    numErrors++;
    return;
  }
  Serial << F("  Original value saved.") << ENDL;
  if (!xbc.setParameter(cmd, newValue)) {
    numErrors++;
    Serial << F("Error") << ENDL;
  } else {
    Serial << F("  Checking value...") << ENDL;
    if (!xbc.queryParameter(cmd, tmpValue)) {
      numErrors++;
      Serial << F(" Error: could not re-read value") << ENDL;
    } else {
      if (tmpValue != newValue) {
        Serial << F(" Error: got ") << tmpValue << F("' instead of ") << newValue  << ENDL;
               numErrors++;
      }
    }
  }
  Serial << F("  Setting ") << cmd << F(" parameter back to saved value...");
  if (!xbc.setParameter(cmd, savedValue)) {
    numErrors++;
    Serial << F("  *** Error") << ENDL;
  } else {
    Serial << F("OK.") << ENDL;
    Serial << F("  Checking value...") << ENDL;
    if (!xbc.queryParameter(cmd, tmpValue)) {
      numErrors++;
      Serial << F(" Error: could not re-read value") << ENDL;
    } else {
      if (tmpValue != savedValue) {
        Serial << F(" Error: got 0x") << tmpValue << F("' instead of ") << savedValue << ENDL;
        numErrors++;
      }
    }
  }
} ; // testSet

void printHexValue(uint8_t value[], uint8_t valueSize, bool addEndLine = true) {
  Serial << "0x";
  for (int i = 0; i < valueSize; i++) {
    if (value[i] < 0x10) Serial << '0';
    Serial.print(value[i], HEX);
  }
  if (addEndLine) Serial.println();
}

void RecordError() {
  Serial << " **** Error ****" << ENDL;
  numErrors++;
}

void testQueryWithHex(const char* param) {
  uint8_t value[MaxValueSize];
  uint8_t valueSize = MaxValueSize;
  if (xbc.queryParameter(param, value, valueSize))  {
    Serial << param << " = ";
    printHexValue(value, valueSize);
  } else numErrors++;
}

void testSetMethods() {
  uint8_t value[] = {'t' , 'e', 's', 't', 'A', 'B', 'C' , '1', '2', '3'};
  char savedName[MaxValueSize];
  Serial << F("Testing SET with HEX values...") << ENDL;

  Serial << F("Setting with invalid parameter name (should fail)...");
  if (xbc.setParameter("NINI", value, 7)) {
    numErrors++;
    Serial << F("Error") << ENDL;
  } else Serial << F(" OK.") << ENDL;

  Serial << F("  Setting NI parameter to 'testABC'...") << ENDL;
  if (!xbc.queryParameter("NI", savedName, MaxValueSize)) {
    numErrors++;
    return;
  }
  Serial << F("  Original value '") << savedName << F("' saved.") << ENDL;
  if (!xbc.setParameter("NI", value, 7)) {
    numErrors++;
    Serial << F("Error") << ENDL;
  } else {
    Serial << F("  Checking value...") << ENDL; ;
    char str[10];
    if (!xbc.queryParameter("NI", str, (uint8_t) 10)) {
      numErrors++;
      Serial << F("  *** Error: could not re-read value") << ENDL;
    } else {
      if (strcmp(str, "testABC") != 0) {
        Serial << F("  *** Error: got '") << str << F("' instead of 'testABC'") << ENDL;
        numErrors++;
      }
    }
  }

  Serial << F("Testing SET with string....") << ENDL;
  Serial << F("  Setting NI parameter back to '") << savedName << "'...";
  if (!xbc.setParameter("NI", savedName)) {
    numErrors++;
    Serial << F("  *** Error") << ENDL;
  } else {
    Serial << F("OK.") << ENDL;
    Serial << F("  Checking value...") << ENDL;
    char str[MaxValueSize];
    if (!xbc.queryParameter("NI", str, MaxValueSize)) {
      numErrors++;
      Serial << F("  *** Error: could not re-read value") << ENDL;
    } else {
      if (strcmp(str, savedName) != 0) {
        Serial << F("  *** Error: got '") << str << F("' instead of '") << savedName << "'" << ENDL;
        numErrors++;
      }
    }
  }

  testSet("ID", (uint64_t) 0x1234546);
  testSet("DL", (uint32_t) 0x1234);
  testSet("SN", (uint16_t) 0xABCD);
  testSet("NH", (uint8_t) 0xCC);
 
  bool savedAO, ao;
  Serial << F("Testing SET with bool...") << ENDL;
  Serial << F("  Changing AO parameter...") << ENDL;
  if (!xbc.queryParameter("AO", savedAO)) {
    numErrors++;
    return;
  }
  Serial << F("  Original value (") << savedAO << F(") saved.") << ENDL;
  if (!xbc.setParameter("AO", !savedAO)) {
    numErrors++;
    Serial << F("Error") << ENDL;
  } else {
    Serial << F("  Checking value...") << ENDL;
    if (!xbc.queryParameter("AO", ao)) {
      numErrors++;
      Serial << F(" Error: could not re-read value") << ENDL;
    } else {
      if (ao == savedAO) {
        Serial << F(" Error: got ") <<  ao << F(" instead of ") << !savedAO << ENDL;
        numErrors++;
      }
    }
  }
  Serial << F("  Setting AO parameter back to ") << savedAO << "...";
  if (!xbc.setParameter("AO", savedAO)) {
    numErrors++;
    Serial << F("  *** Error") << ENDL;
  } else {
    Serial << F("OK.") << ENDL;
    Serial << F("  Checking value...") << ENDL;
    if (!xbc.queryParameter("AO", ao)) {
      numErrors++;
      Serial << F(" Error: could not re-read value") << ENDL;
    } else {
      if (ao != savedAO) {
        Serial << F(" Error: got ") << ao << F("' instead of ") << savedAO << ENDL;
        numErrors++;
      }
    }
  }
} // testSetMethods.

void testQueryMethods() {
  uint8_t value[1];
  uint8_t valueSize = 1;
  uint64_t ID_Value;

  Serial << F("Testing with invalid parameter name (should fail)...");
  if (xbc.queryParameter("IDID", ID_Value)) {
    numErrors++;
    Serial << "Error" << ENDL;
  } else Serial << " OK." << ENDL;

  Serial << F("Testing query with too small value (should fail)...");
  if (xbc.queryParameter("SH", value, valueSize)) {
    numErrors++;
    Serial << "Error" << ENDL;
  } else Serial << " OK." << ENDL;
  valueSize = MaxValueSize;
  testQueryWithHex("SH");
  testQueryWithHex("SL");
  testQueryWithHex("ID");

  Serial << F("Testing numeric conversion: check MANUALLY! ") << ENDL;
  if (!xbc.queryParameter("ID", ID_Value)) numErrors++;
  Serial << F(" MANUAL CHECK: is value '0x") ;
  Serial.print((unsigned long) ID_Value, HEX);
  Serial << F("' identical to the ID value above?") << ENDL;
  numManualChecks++;

  testQueryWithHex("CE");
  Serial << F("Testing boolean conversion: check MANUALLY! ") << ENDL;
  bool bValue;
  if (!xbc.queryParameter("CE", bValue)) numErrors++;
  else {
    if (bValue) Serial << F(" CHECK: CE value is not 0 above") << ENDL;
    else Serial << F(" MANUAL CHECK: CE value is 0 above") << ENDL;
  }
  numManualChecks++;

  testQueryWithHex("NI");
  Serial << F("Testing string conversion: check MANUALLY! ") << ENDL;
  uint8_t strSize = MaxValueSize;
  char  strValue[MaxValueSize];
  if (!xbc.queryParameter("NI", strValue, strSize)) numErrors++;
  else Serial << F(" MANUAL CHECK: NI '") << strValue << "' value is ok" << ENDL;
  numManualChecks++;
}

#ifdef INCLUDE_RESTORATION_TEST
void testResetAndRestoreCommands() {
  Serial << F("Testing Reset & Restore commands...") << ENDL;

  Serial << F("  Saving current name...") << ENDL;
  char savedName[MaxValueSize];
  if (!xbc.queryParameter("NI", savedName, MaxValueSize)) {
    numErrors++;
    return;
  }
  Serial << F("  Original value '") << savedName << "' saved." << ENDL;

  Serial << "Restoring factory configuration...";
  if (!xbc.RestoreDefaultConfiguration()) {
    numErrors++;
    Serial << " *** ERROR" << ENDL;
    return;
  }
  Serial << " OK. Waiting **** REQUIRED? INCLUDE DELAY IN CLIENT ?? ******** ..." << ENDL;
  delay(10000);
  Serial << " Done waiting" << ENDL;
  xbc.printConfigurationSummary(Serial);

  char name[MaxValueSize];
  if (!xbc.queryParameter("NI", name, MaxValueSize)) {
    numErrors++;
    Serial << " *** ERROR" << ENDL;
    return;
  }
  if (strcmp(name, " ") != 0) {
    Serial << "  ***ERROR: unexpected default name: '" << name << "'" << ENDL;
    numErrors++;
    // do not return to perform reset anyway.
  }

  Serial << "Resetting XBee module to revert to saved configuration... " ;
  if (!xbc.softwareReset()) {
    numErrors++;
    Serial << " *** ERROR" << ENDL;
    return;
  }
  Serial << "OK waiting **** REQUIRED? INCLUDE DELAY IN CLIENT ?? ********... " << ENDL;
  delay(1000);
  xbc.printConfigurationSummary(Serial);

  if (!xbc.queryParameter("NI", name, MaxValueSize)) {
    numErrors++;
    Serial << " *** ERROR" << ENDL;
    return;
  }
  if (strcmp(name, savedName) != 0) {
    Serial << "  ***ERROR: unexpected name: '" << name << "' instead of '" << savedName << "'" << ENDL;
    numErrors++;
  }
}
#endif

void testSoftwareReset() {
  // Change name and reset
  Serial << F("Testing SoftwareReset command...") << ENDL;

  Serial << F("  Saving current name...") << ENDL;
  char name[MaxValueSize], savedName[MaxValueSize];
  if (!xbc.queryParameter("NI", savedName, MaxValueSize)) {
    numErrors++;
    return;
  }
  Serial << F("  Original NI value '") << savedName << "'." << ENDL;

  strcpy(name, savedName);
  int len = strlen(name);
  if (name[len - 1] == '_') {
    name[len - 1] = '\0';
    Serial << F("  Removing final '_'...") << ENDL;
  } else {
    if (len > MaxValueSize - 2) {
      Serial << F(" *** Error: name too long to append a character (test aborted) ***") << ENDL;
      numErrors++;
      return;
    }
    Serial << F("  Appending '_'...") << ENDL;
    name[len] = '_';
    name[len + 1] = '\0';
  }

  Serial << F("  Writing new name '") << name << "'..." << ENDL;
  if (!xbc.setParameter("NI", name)) {
    numErrors++;
    return;
  }
  Serial << F("  Configuration before reset: ") << ENDL;
  if (!xbc.printConfigurationSummary(Serial)) {
    numErrors++;
    return;
  }
  Serial << F("  Resetting (FR)...") << ENDL;
  if (!xbc.softwareReset()) {
    numErrors++;
    return;
  }
  Serial << F("  Configuration after reset: ") << ENDL;
  if (!xbc.printConfigurationSummary(Serial)) {
    numErrors++;
    return;
  }

  if (!xbc.queryParameter("NI", name, MaxValueSize)) {
    numErrors++;
    Serial << F(" *** ERROR") << ENDL;
    return;
  }
  if (strcmp(name, savedName) != 0) {
    Serial << F("  ***ERROR: unexpected name: '") << name << F("' instead of '") << savedName << "'" << ENDL;
    numErrors++;
  }
}

void testWriteCommand() {
  // Alternately append or remove a '$' character to the name, in order to undo
  // the change at next run.
  Serial << F("Testing Write command...") << ENDL;

  Serial << F("  Saving current name...") << ENDL;
  char name[MaxValueSize];
  if (!xbc.queryParameter("NI", name, MaxValueSize)) {
    numErrors++;
    return;
  }
  Serial << F("  Original NI value '") << name << "'." << ENDL;

  int len = strlen(name);
  if (name[len - 1] == '$') {
    name[len - 1] = '\0';
    Serial << F("  Removing final '$'...") << ENDL;
  } else {
    if (len > MaxValueSize - 2) {
      Serial << F(" *** Error: name too long to append a character (test aborted) ***") << ENDL;
      numErrors++;
      return;
    }
    Serial << F("  Appending '$'...") << ENDL;
    name[len] = '$';
    name[len + 1] = '\0';
  }

  Serial << F("  Writing new name '") << name << F("'...") << ENDL;
  if (!xbc.setParameter("NI", name)) {
    numErrors++;
    return;
  }
  Serial << F("  Configuration before committing to permanent memory: ") << ENDL;
  if (!xbc.printConfigurationSummary(Serial)) {
    numErrors++;
    return;
  }
  Serial << F("  Committing to permanent memory (WR)...") << ENDL;
  if (!xbc.saveConfiguration()) {
    numErrors++;
    return;
  }
  Serial << F("  Done. Run test program again to undo the permanent change.") << ENDL;
}

void setup() {
  DINIT(115200);
  DASSERT(Serial);
  Serial << F("***** Testing AT Command functions *****") << ENDL;
  Serial << F("Initialising Serials and communications...") << ENDL;
  RF.begin(115200);
  xbc.begin(RF); // no return value

  Serial << F("Initialisation over") << ENDL;


  uint32_t tmpSH;
  xbc.queryParameter("SH", tmpSH);
  Serial << "SH=0x"; Serial.println(tmpSH, HEX);


  testQueryMethods();
  Serial << "Errors so far: " << numErrors << ENDL << ENDL;
  if (!xbc.printConfigurationSummary(Serial)) numErrors++;

  Serial << F("Errors so far: ") << numErrors << ENDL << ENDL;
  testSetMethods();
  Serial << F("Errors so far: ") << numErrors << ENDL << ENDL;

#ifdef INCLUDE_RESTORATION_TEST
  testResetAndRestoreCommands();
  Serial << F("Errors so far: ") << numErrors << ENDL << ENDL;
#endif
  testSoftwareReset();
  Serial << F("Errors so far: ") << numErrors << ENDL << ENDL;

  testWriteCommand();

  Serial << ENDL << "Test over with " << numErrors << " error(s)." << ENDL;
  Serial << F("Warning: ") <<  numManualChecks << F(" results to check manually above! ") << ENDL;
  Serial << F("! Permanent changes made to the XBee configuration will be undone if this program is run again.") << ENDL;
  Serial << F("! First check the NI change persists after XBee reboot.") << ENDL;
}

void loop() { }
