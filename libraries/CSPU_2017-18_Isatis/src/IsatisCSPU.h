/*
 * cansatAsgardCSPU.h 
 * 
 * This files is only used for documentation of the library using Doxygen.
 * Every class in the library should include a tag @ingroup IsatisCSPU 
 * in the class documentation block.
 */

 /** @defgroup IsatisCSPU IsatisCSPU library 
 *  @brief The library of classes specifically developed for the ISATIS project (CSPU's CanSat 2018).
 *  
 *  
 *  _Dependencies_\n
 *  This library requires the following other generic libraries (in addition to hardware-specific libraries,
 *  and standard Arduino libraries, which are not listed here):
 *  - DebugCSPU
 *  - TimeCSPU
 *  - elapsedMillis
 *  - cansatAsgardCSPU
 *  
 *  
 *  _History_\n
 *  The library was created by the 2017-18 Cansat team (ISATIS).
 */
