/*
 * CSPU_Servos.h
 * 
 * This files is only used for documentation of the library using Doxygen.
 * Every class in the library should include a tag @ingroup CSPU_Servos
 * in the class documentation block.
 */

 /** @defgroup CSPU_Servos CSPU_Servos library
 *  @brief The library of classes used to control servos.
 *  
 *  _Dependencies_\n
 *  This library requires the following other generic libraries (in addition to hardware-specific libraries,
 *  and standard Arduino libraries, which are not listed here):
 *  - CSPU_Debug
 *  - CSPU_CansartAsgard
 *  
 *  
 *  _History_\n
 *  The library was created in Sept. 2021, when restructuring the CSPU libraries for
 *  maintainability.
 */

