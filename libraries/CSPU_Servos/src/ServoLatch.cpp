/*
 * ServoLatch.cpp
 */
#if defined(ARDUINO_SAMD_FEATHER_M0_EXPRESS) || defined(ARDUINO_AVR_UNO) || defined(ARDUINO_AVR_MICRO)
    /* v1.1.8 of the Servo library does not support the SAMD51 processor. See note in class comment */

#define DEBUG_CSPU
#include "DebugCSPU.h"
#define DBG_LOCK 0
#define DBG_UNLOCK 0
#define DBG_BEGIN 0
#include "ServoLatch.h"

static constexpr bool AttachDetachEveryTime=true;

bool ServoLatch::begin( const uint8_t PWM_Pin, int minPulseWidth, int maxPulseWidth) {
  thePWM_Pin = PWM_Pin;
  theMinPulseWidth = minPulseWidth;
  theMaxPulseWidth = maxPulseWidth;
  theStatus=ServoLatch::LatchStatus::Undefined;
  DPRINTS(DBG_BEGIN, "minPulseWidth=");
  DPRINTLN(DBG_BEGIN, minPulseWidth );
  DPRINTS(DBG_BEGIN, "maxPulseWidth=");
  DPRINTLN(DBG_BEGIN, maxPulseWidth );

  if (!AttachDetachEveryTime) { 
    myServo.attach(thePWM_Pin, theMinPulseWidth, theMaxPulseWidth );
    DPRINTLN(DBG_BEGIN, "Attaching");
  }
  return true;
}

void ServoLatch::unlock() {
  DPRINTS(DBG_UNLOCK, "ServoLatch::unlock, using pin #");
  DPRINTLN(DBG_UNLOCK, thePWM_Pin);
  if (AttachDetachEveryTime) {
    myServo.attach(thePWM_Pin, theMinPulseWidth, theMaxPulseWidth );
    DPRINTLN(DBG_UNLOCK, "Attaching");
    delay(DelayAfterAttach);
    DPRINTS(DBG_UNLOCK, "Waiting=");
    DPRINTLN(DBG_UNLOCK, DelayAfterAttach);
  }
  myServo.write(unlockPosition);
  DPRINTS(DBG_UNLOCK, "Writing=");
  DPRINTLN(DBG_UNLOCK, unlockPosition);

  if (AttachDetachEveryTime) {
    delay(DelayBeforeDetach);
    DPRINTS(DBG_UNLOCK, "Waiting=");
    DPRINTLN(DBG_UNLOCK, DelayBeforeDetach);
    myServo.detach();
    DPRINTLN(DBG_UNLOCK, "Detaching");
  }
  theStatus = LatchStatus::Unlocked;
}

void ServoLatch::lock() {
  DPRINTS(DBG_LOCK, "ServoLatch::lock, using pin #");
  DPRINTLN(DBG_LOCK, thePWM_Pin);
  if (AttachDetachEveryTime) {
    myServo.attach(thePWM_Pin, theMinPulseWidth, theMaxPulseWidth);
    DPRINTLN(DBG_LOCK, "Attaching");
    delay(DelayAfterAttach);
    DPRINTS(DBG_LOCK, "Waiting=");
    DPRINTLN(DBG_LOCK, DelayAfterAttach);
  }
  myServo.write(lockPosition);
  DPRINTS(DBG_LOCK, "Writing=");
  DPRINTLN(DBG_LOCK, lockPosition);
  
  if (AttachDetachEveryTime) {
    delay(DelayBeforeDetach);
    DPRINTS(DBG_LOCK, "Waiting=");
    DPRINTLN(DBG_LOCK, DelayBeforeDetach);
    myServo.detach();
    DPRINTLN(DBG_LOCK, "Detaching");
  }
  theStatus = LatchStatus::Locked;
}

void ServoLatch::neutral() {
  DPRINTS(DBG_LOCK, "ServoLatch::neutral, using pin #");
  DPRINTLN(DBG_LOCK, thePWM_Pin);
  if (AttachDetachEveryTime) {
    myServo.attach(thePWM_Pin, theMinPulseWidth, theMaxPulseWidth  );
    DPRINTLN(DBG_LOCK, "Attaching");
    delay(DelayAfterAttach);
    DPRINTS(DBG_LOCK, "Waiting=");
    DPRINTLN(DBG_LOCK, DelayAfterAttach);
  }
  myServo.write((unlockPosition + lockPosition) / 2);
  DPRINTS(DBG_LOCK, "Writing=");
  DPRINTLN(DBG_LOCK, (unlockPosition + lockPosition) / 2);
  if (AttachDetachEveryTime) {
    delay(DelayBeforeDetach);
    DPRINTS(DBG_LOCK, "Waiting=");
    DPRINTLN(DBG_LOCK, DelayBeforeDetach);
    myServo.detach();
    DPRINTLN(DBG_LOCK, "Detaching");
  }
  theStatus = LatchStatus::Neutral;
}

ServoLatch::LatchStatus ServoLatch::getStatus() {
  return theStatus;
}

void ServoLatch::configure(int neutralPosition,int amplitude){
  unlockPosition=neutralPosition+(amplitude/2);
  lockPosition=neutralPosition-(amplitude/2);
}
#endif // supported on current board.
