#pragma once
#if defined(ARDUINO_SAMD_FEATHER_M0_EXPRESS) || defined(ARDUINO_AVR_UNO) || defined(ARDUINO_AVR_MICRO)
    /* v1.1.8 of the Servo library does not support the SAMD51 processor. See note in class comment */
#include <Servo.h>
#else
#error "ServoLatch class not supported on current board."
#endif
#include "Arduino.h"

/** @ingroup GMiniCSPU
    @brief Class that allows the servo to be set in three different position : neutral, locked, and unlocked
    rapport :
  Clement and I (Robin) are working on the programming of the micro Servo. Our goal is to make it rotate a defined number of degrees to allow the sub-cans to eject at the right moment.
  To begin with, we learned how to rotate the servo in a simple, class program:
  -using the servo.h library
  -use of the write function
  -using the attach function

  Then we improved our program by adding :
  -a class: ServoLatch
  -a bool begin function that holds the PWM pin number associated with the servo as a constant.
  -a lock function that allows the servo to return to the 0 position defined beforehand.
  -An unlock function that allows the servo to rotate by a defined number of degrees.
  -A neutral function that allows the servo to rotate by a number of degrees equal to half the unlock position.
  -The use of the detach function which allows the power to be cut off when the servo does not need it.
  -A definition of the lock, unlock and neutral positions.
  -A definition of the waiting time before detach.
  -A definition of the waiting time after attach.
  -a definition of the PWM pin associated with the servo.
  -debugging.

  Finally, we finalized our program by adding :
  -an enum class: LatchStatus.
  -an object of class LatchStatus: getStatus.
  -an object of class LatchStatus: theStatus.
  -a declaration of theMinPulseWidth.
  -a declaration of theMaxPulseWidth.
  -to the bool begin the declarations of the minPulseWidth and maxPulseWidth and their default values.
  -debugging.

  Our program is now ready for use, but we are still working on the documentation.
  The only problem we are facing is that one type of micro servo (SG90) requires non-default values for minPulseWidth and maxPulseWidth. We haven't found the perfect values yet.
  We do have a micro servo (SG92R) that works perfectly with the default minPulseWidth and maxPulseWidth values.

*/

class ServoLatch {
  public :
    /** Enum values representing different servo status*/
    enum class LatchStatus {
      Neutral = 0,
      Locked = 1,
      Unlocked = 2,
      Undefined = 3
    };
    /** Method to call before any other method. Initializes the servo.
        @param PWM_Pin the PWM Pin number of the servo
        @return true if servo initialisation succeeded
    */
    bool begin (const uint8_t PWM_Pin, int minPulseWidth = 544, int maxPulseWidth = 2400);
    void unlock();
    void lock();
    void neutral(); /**< Method for returning the Servo to the neutral position*/ 
    void configure(int neutralPosition,int amplitude);
    

    LatchStatus getStatus(); /**< The LatchStatus object*/


  private :
    Servo myServo; /**< The arduino Servo object*/
    int unlockPosition = 30; /**< Position (in degrees) of the servo when unlocked*/
    int lockPosition = 0; /**< Position (in  degrees) of the servo when locked*/
    static constexpr uint16_t DelayBeforeDetach = 200; /**< Delay (in millisecond) of waiting before detach*/
    static constexpr uint16_t DelayAfterAttach = 200; /**< Delay (in millisecond) of waiting after attach*/
    LatchStatus theStatus; /**< The enum class object*/
    uint8_t thePWM_Pin; /**< Declaration of the thePWM_Pin*/
    int theMinPulseWidth; /**< Declaration of the theMinPulseWidth */
    int theMaxPulseWidth; /**< Declaration of the theMaxPulseWidth */
};
