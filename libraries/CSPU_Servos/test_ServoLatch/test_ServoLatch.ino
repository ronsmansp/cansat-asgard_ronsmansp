#define DEBUG_CSPU
#define USE_ASSERTIONS
#include "DebugCSPU.h"
#include "ServoLatch.h"

ServoLatch myServo;

void setup() {
  DINIT(115200);
  myServo.begin(9, 544, 2400); //microServo SG90: default values, and 544,2000 fail. 600, 2000 ok.
  myServo.configure(90,30);
  DASSERT(myServo.getStatus() == ServoLatch::LatchStatus::Undefined);
}

void loop() {
  myServo.unlock();
  DASSERT(myServo.getStatus() == ServoLatch::LatchStatus::Unlocked);
  delay(3000);
  myServo.lock();
  DASSERT(myServo.getStatus() == ServoLatch::LatchStatus::Locked);
  delay(3000);
  myServo.neutral();
  DASSERT(myServo.getStatus() == ServoLatch::LatchStatus::Neutral);
  delay(3000);

}
